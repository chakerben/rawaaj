$(function () {
   $('.login-link, #overlay').on('click', function (e) {
       e.preventDefault();
       $('.login-wrapper').fadeToggle();
       $('#overlay').fadeToggle();
       if ($(window).width() < 768)
            $('html,body').animate({scrollTop: $('.login-wrapper').offset().top}, 1000);
   });

    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });
});