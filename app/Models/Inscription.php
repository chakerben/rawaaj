<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inscription extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function scopeInscriptionsFilter($query, $user_id, $product_id, $created_at)
    {
        if ($user_id or $product_id or $created_at) {
            $query->where(function ($query) use ($user_id, $product_id, $created_at) {
                $query->Where("type", "=",1);
                if ($user_id)
                    $query->Where("user_id", "=",$user_id);
                if ($product_id)
                    $query->Where("product_id", "=",$product_id);
                if ($created_at)
                    $query->Where("created_at", "LIKE", "%$created_at%");
            });
        }
        return $query;
    }

    public function scopeVisitorsFilter($query, $user_id, $product_id, $created_at)
    {
        if ($user_id or $product_id or $created_at) {
            $query->where(function ($query) use ($user_id, $product_id, $created_at) {
                $query->Where("type", "=",0);
                if ($user_id)
                    $query->Where("user_id", "=",$user_id);
                if ($product_id)
                    $query->Where("product_id", "=",$product_id);
                if ($created_at)
                    $query->Where("created_at", "LIKE", "%$created_at%");
            });
        }
        return $query;
    }
}
