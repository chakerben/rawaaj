<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\UsrResetPasswordNotification;
use App\Models\Inscription;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','code',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%")
                    ->orWhere("email", "LIKE", "%$keyword%")
                    ->orWhere("phone", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new UsrResetPasswordNotification($token));
    }

    public function products(){

        return $this->belongsToMany('App\Models\Product');
    }

    public function num_inscriptions(){
        $inscriptions = Inscription::where("user_id", $this->id)->where("type", 1)->where("status", 1)->count();
        return $inscriptions;
    }

    public function num_visits(){
        $visits = Inscription::where("user_id", $this->id)->where("type", 0)->count();
        return $visits;
    }

    public function total(){
        $visits = Inscription::where("user_id", $this->id)->where("type", 1)->where("status", 1)->sum("final_price");
        return $visits;
    }
}
