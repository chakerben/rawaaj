<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Payment extends Model
{
    public function product(){
        return $this->BelongsTo("App\Models\Product");
    }

    public function user(){
        return $this->BelongsTo("App\Models\User");
    }

    public function inscription(){
        return $this->BelongsTo("App\Models\Inscription");
    }
}
