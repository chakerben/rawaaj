<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\user;
use Auth;

class Product extends Model
{

    public function users(){

        return $this->belongsToMany('App\Models\User')->withPivot('url', 'short_url');
    }

    public function getPivotedAttribute(){
        $user = Auth::user();
        return $user->products()->where("products.id", $this->id)->exists();
    }
}
