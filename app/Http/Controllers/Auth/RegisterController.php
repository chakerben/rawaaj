<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Support\Facades\Auth as Authi;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Mail;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:user');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'g-recaptcha-response' => 'required|captcha',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'phone' => 'max:20',
            'conditions'=>'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    protected function create(array $data)
    {
        $code = unique_id(6);
        $users = User::all();
        $codes=[];
        foreach ($users as $user)array_push($codes, $user->code);
        if(!empty($codes)){
            while (in_array($code, $codes))
                $code = unique_id(6);
        }

        $confirmation_code = str_random(30);
        $user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'phone' => $data['phone'],
            'code' => $code,
        ]);

        $userx = User::find($user->id);
        $userx->confirmation_code = $confirmation_code;
        $userx->save();

        Mail::send('email_verify', ['confirmation_code'=>$confirmation_code], function($message) use($data) {
            $message->to($data['email'], $data['name'])
                ->subject('كود التحقق');
        });
        return $user;
    }

    public function confirm($confirmation_code)
    {
        if (!$confirmation_code) {
            return redirect(route('home'));
        }

        $user = User::where("confirmation_code",$confirmation_code)->first();

        if ( ! $user)
        {
            return redirect(route('home'));
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();
        Authi::login($user);

        return redirect()->route('dashboard')->with("status", "لقد فعلت حسابك بنجاح.");
    }

    protected function guard()
    {
        return Authi::guard("web");
    }
}
