<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:web')->except('logout');
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        $user = $this->guard()->user();
        $user->last_login = date("Y-m-d");
        $user->save();

        return $this->authenticated($request, $user)
            ?: redirect()->intended($this->redirectPath());
    }

    protected function credentials(Request $request)
    {
        return array_merge($request->only($this->username(), 'password'), ["status"=>1]);
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'g-recaptcha-response' => 'required|captcha',
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    protected function guard()
    {
        return Auth::guard("web");
    }

    public function logout(Request $request)
    {
        Auth::guard('web')->logout();

        //$request->session()->invalidate();

        return redirect()->route("home");
    }
}
