<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Inscription;
use App\Models\User;
use App\Models\Product;
use App\Models\Country;
use Response;
use Hash;

class Api extends Controller
{
    public function save(Request $request, $t,$u, $p, $n,$e,$ip=null)
    {
        if ($u and $p) {
            $user = User::where("code", $u)->first();
            $product = Product::where("ref", $p)->first();

            if($user and $product){

                $check = Inscription::where("type", $t)->where("product_id", $product->id)->where("user_id", $user->id)->where("ip", $ip)->first();
                if($check)
                    return;

                $inscription = new Inscription;
                $inscription->user_id = $user->id;
                $inscription->product_id = $product->id;
                $inscription->name = $n;
                $inscription->email = $e;
                $inscription->status = 0;
                $inscription->ip = $ip;
                $inscription->type = $t;
                $inscription->price = $product->price;
                if($user->percentage and $user->percentage!=""){
                    $inscription->percentage = $user->percentage;
                    $inscription->final_price = number_format((float)($product->price/100)*$user->percentage,2);
                }else{
                    $inscription->percentage = $product->percentage;
                    $inscription->final_price = number_format((float)($product->price/100)*$product->percentage,2);
                }

                try {
                    $visitor_ip_details = json_decode(@file_get_contents("http://ipinfo.io/{$ip}/json"));
                    $visitor_city = @$visitor_ip_details->city;
                    if ($visitor_city == "") {
                        $inscription->city = "";
                    }else
                        $inscription->city = $visitor_city;
                    $visitor_country_code = @$visitor_ip_details->country;
                    if ($visitor_country_code == "") {
                        $inscription->country_code = "";
                        $inscription->country = "";
                    } else {
                        $v_country = Country::where('code', '=', $visitor_country_code)->first();
                        $inscription->country_code = $visitor_country_code;
                        $inscription->country = $v_country->title_ar;
                    }
                } catch (Exception $e) {
                    $inscription->city = "";
                    $inscription->country_code = "";
                    $inscription->country = "";
                }

                $inscription->save();

                return Response::json(["user"=>$user->name, "product"=>$product->name])->setCallback('callback');

            }
        }
    }
}
