<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Setting;
use App\Models\Contact;
use Auth;
use Mail;

class front extends Controller
{
    public function home()
    {
        $settings = Setting::all()->keyBy('id');

        return view('front.home', compact("settings"));
    }

    public function conditions(){
        $conditions = @Setting::find(4)->value;

        return view('front.conditions', compact("conditions"));
    }

    public function about_us(){
        $content = @Setting::find(10)->value;

        return view('front.about_us', compact("content"));
    }

    public function contact_us(){
        $email = @Setting::find(6)->value;
        $phone = @Setting::find(7)->value;
        $fax = @Setting::find(8)->value;
        $address = @Setting::find(9)->value;

        return view('front.contact_us', compact("email", "phone","fax","address"));
    }

    public function save_contact_us(Request $request){
        $this->validate($request, [
            'g-recaptcha-response' => 'required|captcha',
            'name'=>'required',
            'email'=>'required|email',
            'subject'=>'required',
            'message'=>'required',
        ]);

        $administration_email = @Setting::find(6)->value;

        $contact = new Contact;
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->subject = $request->subject;
        $contact->message = $request->message;
        $contact->save();

        Mail::raw($request->message, function ($message) use ($administration_email, $request){
            $message->to($administration_email);
            $message->from($request->email, config("app.name"));
            $message->subject($request->subject);
        });

        return redirect()->back()->with("status", "تم إرسال الرسالة للإدارة");
    }

}
