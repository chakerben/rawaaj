<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Inscription;
use App\Models\Message;
use App\Models\Payment;
use Auth;
use Mail;
use Purl\Url as Purl;
use Shorty;


class Dashboard extends Controller
{

    public function __construct()
    {
        $this->middleware("auth:web")->except("product");
    }

    public function index()
    {
        $user = Auth::user();
        if($user->confirmed==0)
            return view('front.dashboard.unconfirmed');
        $products = Product::where("published", 1)->get();
        $visitors = Inscription::where("type", 0)->where("user_id", $user->id)->orderBy('created_at', 'desc')->get();
        $inscriptions = Inscription::where("type", 1)->where("user_id", $user->id)->orderBy('created_at', 'desc')->get();
        $messages = Message::where("user_id", $user->id)->where("response", 0)->with("messages","user")->get();
        $payments = Payment::orderby('id', 'asc')->where("user_id", $user->id)->with("product")->get();
        $last_payement = Payment::orderby('created_at', 'desc')->where("user_id", $user->id)->where("status", 1)->first();

        return view('front.dashboard.index', compact("user", "products", "visitors", "inscriptions", "messages", "payments", "last_payement"));
    }

    public function save_infos(Request $request)
    {
        $id = Auth::user()->id;
        $email = Auth::user()->email;
        if ($request->email != $email)
            $this->validate($request, ["email" => 'required|unique:users',]);
        $this->validate($request, [
            'g-recaptcha-response' => 'required|captcha',
            "email" => 'required|email',
            "name" => 'required',
            "phone" => 'required',
            "account"=>'required'
        ]);

        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->age = $request->age;
        $user->gender = $request->gender;
        $user->city = $request->city;
        $user->account= $request->account;
        $user->availability = $request->availability;
        $user->interests = $request->interests;
        $user->save();

        return redirect()->back()->with("status", "تم تعديل البيانات الشخصية");
    }

    public function product($id)
    {
        $user = Auth::user();
        $product = Product::find($id);

        return view('front.dashboard.product', compact("user", "product"));
    }

    public function product_user($id)
    {
        $user = Auth::user();
        $product = Product::find($id);

        $has_product = $user->products()->where("products.id", $id)->exists();
        if(!$has_product)
        {
            $url = Purl::parse($product->url);
            $url->query->setData(array(
                'RU' => $user->code,
                'RP' => $product->ref
            ));
            $short_url = Shorty::shorten($url->getUrl());
            $user->products()->attach($id,["url"=>$url->getUrl(),"short_url"=>$short_url]);
        }

        $product = Product::with(["users"=>function($q)use($user){$q->where("users.id", $user->id);}])->where("id", $id)->first();
        $url = @$product->users[0]->pivot->url;
        $short_url = @$product->users[0]->pivot->short_url;

        $visitors = Inscription::where("type", 0)->where("user_id", $user->id)->where("product_id", $id)->orderBy('created_at', 'desc')->get();
        $inscriptions = Inscription::where("type", 1)->where("user_id", $user->id)->where("product_id", $id)->orderBy('created_at', 'desc')->get();

        return view('front.dashboard.product_user', compact("user", "product", "url", "short_url", "visitors", "inscriptions"));
    }

    public function ajax($id)
    {
        $user = Auth::user();
        $product = Product::find($id);
        $url = Purl::parse($product->url);
        $url->query->setData(array(
            'RU' => $user->code,
            'RP' => $product->ref
        ));
        $short_url = Shorty::shorten($url->getUrl());
        echo json_encode(["url" => $url->getUrl(), "short_url" => $short_url]);
    }

    public function send(Request $request){
        $this->validate($request, [
            'g-recaptcha-response' => 'required|captcha',
            'subject'=>'required',
            'message'=>'required',
        ]);

        $administration_email = @Setting::find(6)->value;
        $user = Auth::user();

        $message = new Message;

        if($request->hasFile("file")){
            $file = time() . rand(1111, 9999) . '.' . $request->file->getClientOriginalExtension();
            $request->file->move("public/uploads/messages/", $file);
            $message->file = $file;
        }
        $message->response=0;
        $message->user_id=$user->id;
        $message->subject = $request->subject;
        $message->message = $request->message;
        $message->save();

        if($message->file){
            Mail::send('email_template',
                ['text'=>$request->message, 'file'=>$message->file]
                , function ($mes) use ($administration_email, $user, $request){
                $mes->to($administration_email);
                $mes->from($user->email, config("app.name"));
                $mes->subject($request->subject);
            });
        }else{
            Mail::raw($request->message, function ($mes) use ($administration_email, $user, $request){
                $mes->to($administration_email);
                $mes->from($user->email, config("app.name"));
                $mes->subject($request->subject);
            });
        }

        return redirect()->back()->with("status", "تم إرسال الرسالة للإدارة");
    }

    public function show_responses($id){
        $message = Message::with("user", "messages")->find($id);

        return view("front.dashboard.show_responses", compact("message"));
    }

    public function resend(Request $request, $id){
        $this->validate($request, [
            'g-recaptcha-response' => 'required|captcha',
            'message'=>'required',
        ]);

        $administration_email = @Setting::find(6)->value;
        $user = Auth::user();

        $message = new Message;
        if($request->hasFile("file")){
            $file = time() . rand(1111, 9999) . '.' . $request->file->getClientOriginalExtension();
            $request->file->move("public/uploads/messages/", $file);
            $message->file = $file;
        }
        $message->response=1;
        $message->response_from = $user->id;
        $message->user_id=$user->id;
        $message->message_id=$id;
        $message->subject = "رد المسوق ".$user->name;
        $message->message = $request->message;
        $message->status = 1;
        $message->save();

        if($message->file){
            Mail::send('email_template',
                ['text'=>$request->message, 'file'=>$message->file]
                , function ($mes) use ($administration_email, $user, $request) {
                    $mes->to($administration_email);
                    $mes->from($user->email, config("app.name"));
                    $mes->subject("رد المسوق " . $user->name);
                });
        }else {
            Mail::raw($request->message, function ($mes) use ($administration_email, $user, $request) {
                $mes->to($administration_email);
                $mes->from($user->email, config("app.name"));
                $mes->subject("رد المسوق " . $user->name);
            });
        }
        return redirect()->back()->with("status", "تم إرسال الرسالة للإدارة");
    }
}
