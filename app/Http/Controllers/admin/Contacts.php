<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Models\Contact;
use Auth;

class Contacts extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next)
        {
            $role = (Auth::user())?Auth::user()->role:1;
            if($role==0 && !in_array(8, @$this->roless))
                return redirect()->route('admin.dashboard');
            return $next($request);
        });
    }

    public function index()
    {
        $contacts = Contact::orderBy('created_at', 'desc')->get();

        return view("admin.contacts.index", compact("contacts"));
    }

    public function show($id){
        $contact = Contact::find($id);
        $contact->read=1;
        $contact->save();

        return view("admin.contacts.show", compact("contact"));
    }

    public function delete($id){
        $contact = Contact::find($id);
        $contact->delete();

        return redirect()->back()->with("success", "تم حذف الرسالة");
    }

}
