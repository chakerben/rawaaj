<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Models\Payment;
use Auth;

class Payments extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next)
        {
            $role = (Auth::user())?Auth::user()->role:1;
            if($role==0 && !in_array(10, @$this->roless))
                return redirect()->route('admin.dashboard');
            return $next($request);
        });
    }

    public function index(){
        $payments = Payment::orderby('id', 'asc')->with("product")->get();
        $last_payement = Payment::orderby('created_at', 'desc')->where("status", 1)->first();

        return view("admin.payments.index", compact("payments", "last_payement"));
    }

    public function pay($id){
        $payment = Payment::find($id);
        $payment->status = 1;
        $payment->save();

        return redirect()->back()->with("success", "تم تعديل البيانات بنجاح");
    }

    public function delete($id){
        $payment = Payment::find($id);
        $payment->delete();

        return redirect()->back()->with("success", "تم حذف البيانات بنجاح");
    }
}
