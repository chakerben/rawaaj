<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Models\Message;
use App\Models\User;
use App\Models\Setting;
use Mail;
use Auth;

class Messages extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next)
        {
            $role = (Auth::user())?Auth::user()->role:1;
            if($role==0 && !in_array(7, @$this->roless))
                return redirect()->route('admin.dashboard');
            return $next($request);
        });
    }

    public function index()
    {
        $messages = Message::where("response",0)->with("user")->get();

        return view("admin.messages.index", compact("messages"));
    }

    public function response($id){
        $message = Message::find($id);
        $message->status=1;
        $message->save();

        $message = Message::with("user", "messages")->find($id);

        return view("admin.messages.response", compact("message"));
    }

    public function send_response(Request $request, $id){
        $messagex = Message::with("user", "messages")->find($id);

        $email_to = $messagex->user->email;
        $administration_email = @Setting::find(6)->value;

        $message = new Message;
        if($request->hasFile("file")){
            $file = time() . rand(1111, 9999) . '.' . $request->file->getClientOriginalExtension();
            $request->file->move("public/uploads/messages/", $file);
            $message->file = $file;
        }

        $message->response=1;
        $message->response_from = 0;
        $message->message_id=$messagex->id;
        $message->user_id=$messagex->user_id;
        $message->subject = "رد على رسالة";
        $message->message = $request->response;
        $message->status = 1;
        $message->save();

        if($message->file){
            Mail::send('email_template',
                ['text'=>$request->response, 'file'=>$message->file]
                , function ($mes) use ($administration_email, $email_to, $request) {
                    $mes->to($email_to);
                    $mes->from($administration_email, config("app.name"));
                    $mes->subject("رد على رسالة");
                });
        }else {
            Mail::raw($request->response
                , function ($mes) use ($administration_email, $email_to, $request) {
                $mes->to($email_to);
                $mes->from($administration_email, config("app.name"));
                $mes->subject("رد على رسالة");
            });
        }
        return redirect()->back()->with("success", "تم الإرسال بنجاح");
    }

    public function delete($id){
        $message = Message::find($id);
        $message->delete();

        return redirect()->back()->with("success", "تم حذف الرسالة");
    }

}
