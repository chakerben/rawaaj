<?php

namespace App\Http\Controllers\admin\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use App\Models\Admin;

class Authentification extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = 'admin';

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function login()
    {
        return view("admin.auth.login");
    }

    public function do_login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        return $this->sendFailedLoginResponse($request);
    }


    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();

        //$request->session()->invalidate();

        return redirect()->route("admin.auth.login");
    }

    protected function credentials(Request $request)
    {
        return array_merge($request->only($this->username(), 'password'), ["status"=>1]);
    }


    protected function guard()
    {
        return Auth::guard("admin");
    }


}
