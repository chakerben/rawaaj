<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Setting;
use Auth;

class Admins extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next)
        {
            $role = (Auth::user())?Auth::user()->role:1;
            if($role==0 && !in_array(2, @$this->roless))
                return redirect()->route('admin.dashboard');
            return $next($request);
        });
    }

    public function index()
    {
        $admins = Admin::orderby('id', 'asc')->get();

        return view("admin.admins.index", compact("admins"));
    }

    public function create()
    {
        return view("admin.admins.create");
    }

    public function store(Request $request){
        $admin = new Admin;
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->password = bcrypt($request->password);
        $admin->role = $request->role;
        $admin->status = $request->status;
        $admin->save();

        return redirect(route("admin.admins.index"))->with("success", "تم إضافة البيانات بنجاح");
    }

    public function edit($id){
        $admin = Admin::find($id);

        return view("admin.admins.edit", compact("admin"));
    }

    public function update(Request $request, $id){
        $admin = Admin::find($id);
        $admin->name = $request->name;
        $admin->email = $request->email;
        if($request->password)
            $admin->password = bcrypt($request->password);
        $admin->role = $request->role;
        $admin->status = $request->status;
        $admin->save();

        return redirect()->back()->with("success", "تم تعديل البيانات بنجاح");
    }

    public function delete($id){
        $admin = Admin::find($id);
        if($admin)
            $admin->delete();

        return redirect(route("admin.admins.index"))->with("success", "تم حذف البيانات بنجاح");
    }

    public function roles(){
        $roles = Setting::find(11);
        $roles = explode(",", @$roles->value);
        if(!is_array($roles))
            $roles = ["null"];

        return view("admin.admins.roles", compact("roles"));
    }

    public function save_roles(Request $request){
        $roles = (@$request->roles)?implode(",", @$request->roles):null;
        $roles_settings = Setting::find(11);
        $roles_settings->value = $roles;
        $roles_settings->save();

        return redirect()->back()->with("success", "تم حفظ البيانات بنجاح");

    }
}
