<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Inscription;
use App\Models\Product;
use App\Models\Payment;
use App\Models\User;
use DB;
use Auth;

class Dashboard extends AdminController
{
    public function dashboard()
    {
        $users = User::count();
        $products = Product::count();
        $inscriptions = Inscription::where("type", 1)->count();
        $visitors = Inscription::where("type", 0)->count();
        $insc = Inscription::selectRaw('count(*) AS total, country_code')->where("type", 1)->groupBy('country_code')->get();
        $insc2 = Inscription::selectRaw('count(*) AS total, country_code')->where("type", 0)->groupBy('country_code')->get();
        $best = Inscription::selectRaw('count(*) AS total, user_id')->where("type", 1)->groupBy('user_id')->orderByRaw('COUNT(*) DESC')->limit(4)->get();
        foreach ($best as $item){
            $user=User::find($item->user_id);
            $item->user_id = @$user->name;
        }
        $best = $best->reverse();

        return view("admin.dashboard",
            compact("users", "products", "inscriptions", "visitors", "insc", "insc2", "best"));
    }

    public function inscriptions(){
        if(Auth::user()->role==0 && !in_array(5, @$this->roless))
            return redirect()->route('admin.dashboard');

        $inscriptions = Inscription::where("type", 1)->with("user", "product")->orderBy('created_at', 'desc')->get();
        $users = User::all();
        $products = Product::all();

        return view("admin.inscriptions", compact("inscriptions", "users", "products"));
    }

    public function inscriptions_filter(Request $request){
        if(Auth::user()->role==0 && !in_array(6, @$this->roless))
            return redirect()->route('admin.dashboard');

        $created_at = ($request->created_at)?date("Y-m-d", strtotime($request->created_at)):null;
        $inscriptions = Inscription::InscriptionsFilter($request->user_id, $request->product_id, $created_at)->get();
        $users = User::all();
        $products = Product::all();

        $user_id = $request->user_id;
        $product_id = $request->product_id;
        $created_at = $request->created_at;

        return view("admin.inscriptions", compact("inscriptions", "users", "products", "user_id", "product_id", "created_at"));
    }

    public function inscription_status($id, $status){
        $inscription = Inscription::find($id);
        if($status==2){
            $inscription->delete();

            return redirect(route("dashboard.inscriptions"))->with("success", "تم حذف البيانات بنجاح");
        }else{
            $product = Product::find($inscription->product_id);
            $user = User::find($inscription->user_id);
            
            $payment = new Payment;
            $payment->user_id = $inscription->user_id;
            $payment->product_id = $inscription->product_id;
            $payment->inscription_id = $id;
            $payment->price = $product->price;
            if($user->percentage and $user->percentage!=""){
                $payment->percentage = $user->percentage;
                $payment->final_price = number_format((float)($product->price/100)*$user->percentage,2);
            }else{
                $payment->percentage = $product->percentage;
                $payment->final_price = number_format((float)($product->price/100)*$product->percentage,2);
            }
            $payment->status = 0;
            $payment->save();

            $inscription->status = $status;
            $inscription->save();

            return redirect()->back()->with("success", "تم تسجيل قبول الإشتراك و إنشاء تصفية جديدة في قاعدة البيانات بنجاح");
        }
    }

    public function visitors(){
        if(Auth::user()->role==0 && !in_array(6, @$this->roless))
            return redirect()->route('admin.dashboard');

        $visitors = Inscription::where("type", 0)->with("user", "product")->orderBy('created_at', 'desc')->get();
        $users = User::all();
        $products = Product::all();

        return view("admin.visitors", compact("visitors", "users", "products"));
    }

    public function visitors_filter(Request $request){
        if(Auth::user()->role==0 && !in_array(6, @$this->roless))
            return redirect()->route('admin.dashboard');

        $created_at = ($request->created_at)?date("Y-m-d", strtotime($request->created_at)):null;
        $visitors = Inscription::VisitorsFilter($request->user_id, $request->product_id, $created_at)->get();
        $users = User::all();
        $products = Product::all();

        $user_id = $request->user_id;
        $product_id = $request->product_id;
        $created_at = $request->created_at;

        return view("admin.visitors", compact("visitors", "users", "products", "user_id", "product_id", "created_at"));
    }
}
