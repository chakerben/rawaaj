<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use File;
use Auth;

class Products extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next)
        {
            $role = (Auth::user())?Auth::user()->role:1;
            if($role==0 && !in_array(4, @$this->roless))
                return redirect()->route('admin.dashboard');
            return $next($request);
        });
    }

    public function index()
    {
        $products = Product::all();

        return view("admin.products.index", compact("products"));
    }

    public function create()
    {
        return view("admin.products.create");
    }

    public function store(Request $request)
    {
        $code = unique_id(6);
        $prods = Product::all();
        $codes = [];
        foreach ($prods as $prod) array_push($codes, $prod->ref);
        if (!empty($codes)) {
            while (in_array($code, $codes))
                $code = unique_id(6);
        }

        $product = new Product;
        if ($request->hasFile('image')) {
            $image = time() . rand(1111, 9999) . '.' . $request->image->getClientOriginalExtension();
            $request->image->move("public/uploads/images/", $image);
            $product->image = $image;
        }
        $product->name = $request->name;
        $product->price = $request->price;
        $product->short_description = $request->short_description;
        $product->description = $request->description;
        $product->url = $request->url;
        $product->status = $request->status;
        $product->percentage = $request->percentage;
        $product->published = $request->published;
        $product->ref = $code;
        $product->save();

        return redirect(route("admin.products.index"))->with("success", "تم إضافة البيانات بنجاح");
    }

    public function edit($id)
    {
        $product = Product::find($id);

        return view("admin.products.edit", compact("product"));
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        if ($request->hasFile('image')) {
            File::delete("public/uploads/images/" . $product->image);
            $image = time() . rand(1111, 9999) . '.' . $request->image->getClientOriginalExtension();
            $request->image->move("public/uploads/images/", $image);
            $product->image = $image;
        }
        $product->name = $request->name;
        $product->price = $request->price;
        $product->short_description = $request->short_description;
        $product->description = $request->description;
        $product->url = $request->url;
        $product->status = $request->status;
        $product->percentage = $request->percentage;
        $product->published = $request->published;
        $product->save();

        return redirect(route("admin.products.index"))->with("success", "تم إضافة البيانات بنجاح");
    }

    public function delete($id)
    {
        $product = Product::find($id);
        if ($product){
            $product->delete();
            File::delete("public/uploads/images/" . $product->image);
        }

        return redirect(route("admin.products.index"))->with("success", "تم حذف البيانات بنجاح");
    }

}
