<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Models\Message;
use App\Models\Contact;
use App\Models\Setting;


class AdminController extends Controller
{
    protected $roless = [];
    protected $user;
    public function __construct()
    {
        $this->middleware("auth:admin");
        $messages = Message::where("status", 0)->get();
        View::share('msgs', $messages);
        $contacts = Contact::where("read", 0)->get();
        View::share('contcts', $contacts);
        $roles = Setting::find(11);
        $roles = explode(",", @$roles->value);
        if(!is_array($roles))
            $roles = ["null"];
        $this->roless = $roles;
        View::share('roless', $roles);
    }
}
