<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Models\Setting;
use Auth;

class Settings extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next)
        {
            $role = (Auth::user())?Auth::user()->role:1;
            if($role==0 && !in_array(9, @$this->roless))
                return redirect()->route('admin.dashboard');
            return $next($request);
        });
    }

    public function index(){
        $settings = Setting::all()->keyBy('id');

        return view("admin.settings.index", compact("settings"));
    }

    public function save_content(Request $request){
        foreach ($request->except("_token") as $k=>$v){
            $ss = Setting::find($k);
            $ss->value = $v;
            $ss->save();
        }

        return redirect()->back()->with("success", "تم تعديل البيانات بنجاح");
    }
}
