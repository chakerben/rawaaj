<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\User;
use App\Models\Product;
use App\Models\Inscription;
use App\Models\Payment;
use Auth;

class Users extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next)
        {
            $role = (Auth::user())?Auth::user()->role:1;
            if($role==0 && !in_array(3, @$this->roless))
                return redirect()->route('admin.dashboard');
            return $next($request);
        });
    }

    public function index()
    {
        $keyword = Input::get('keyword', '');
        if ($keyword)
            $users = User::SearchByKeyword($keyword)->get();
        else
            $users = User::orderby('id', 'asc')->get();

        return view("admin.users.index", compact("users", "keyword"));
    }

    public function create()
    {
        return view("admin.users.create");
    }

    public function store(Request $request)
    {
        $code = unique_id(6);
        $users = User::all();
        $codes=[];
        foreach ($users as $user)array_push($codes, $user->code);
        if(!empty($codes)){
            while (in_array($code, $codes))
                $code = unique_id(6);
        }

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);
        $user->status = $request->status;
        $user->code = $code;
        $user->percentage = @$request->percentage;
        $user->save();

        return redirect(route("admin.users.index"))->with("success", "تم إضافة البيانات بنجاح");
    }

    public function edit($id)
    {
        $user = User::find($id);

        return view("admin.users.edit", compact("user"));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        if ($request->password)
            $user->password = bcrypt($request->password);
        $user->status = $request->status;
        $user->percentage = @$request->percentage;
        $user->save();

        return redirect()->back()->with("success", "تم تعديل البيانات بنجاح");
    }

    public function delete($id)
    {
        $user = User::find($id);
        if ($user)
            $user->delete();

        return redirect(route("admin.users.index"))->with("success", "تم حذف البيانات بنجاح");
    }

    public function payments($id){
        $user = User::find($id);
        $inscriptions = Inscription::where("type", 1)->where("user_id", $user->id)->with("product")->orderBy('created_at', 'desc')->get();
        $payments = Payment::orderby('id', 'asc')->where("user_id", $user->id)->with("product")->get();
        $last_payement = Payment::orderby('created_at', 'desc')->where("user_id", $user->id)->where("status", 1)->first();

        return view("admin.users.payments", compact("user", "inscriptions", "payments", "last_payement"));
    }
}
