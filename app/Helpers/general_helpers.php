<?php

if(!function_exists("assetAdmin")){
    function assetAdmin($path){
        echo asset("public/Admin/".$path);
    }
}

if(!function_exists("assetFront")){
    function assetFront($path){
        echo asset("public/Front/".$path);
    }
}

if(!function_exists("unique_id")){
    function unique_id($l = 8) {
        return substr(md5(uniqid(mt_rand(), true)), 0, $l);
    }
}

