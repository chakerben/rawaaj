<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'يجب أن تتكون كلمة المرور من ستة أحرف على الأقل وأن تكون مطابقة لحقل التأكيد',
    'reset' => 'تمت إعادة تعيين كلمة المرور!',
    'sent' => 'تم إرسال رابط إعادة تعيين كلمة المرور الخاصة بك!',
    'token' => 'رمز إعادة تعيين كلمة المرور هذا غير صالح',
    'user' => "لا يمكننا العثور على مستخدم لديه عنوان البريد الإلكتروني هذا",

];
