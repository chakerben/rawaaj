@extends('admin.app')

@section('title', 'الإعدادات')
@section('menu', 9)

@section('content')

    <section class="content-header">
        <h1>
            الإعدادات
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
            <li><i class="fa fa-gears"></i> الإعدادات</li>
        </ol>
    </section>

    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
                <li class="active"><a href="#tab_1-0" data-toggle="tab">إعدادات عامة</a></li>
                <li><a href="#tab_1-1" data-toggle="tab">المحتوى</a></li>
                <li><a href="#tab_2-2" data-toggle="tab">التسويق</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1-0">
                    @include("admin.settings.form0")
                </div>
                <div class="tab-pane" id="tab_1-1">
                    @include("admin.settings.form1")
                </div>
                <div class="tab-pane" id="tab_2-2">
                    @include("admin.settings.form2")
                </div>
            </div>
        </div>
    </section>

@endsection
