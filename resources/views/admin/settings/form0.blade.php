<form action="{{route('admin.settings.save_content')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
    {{csrf_field()}}

    <div class="box-body">
        <div class="form-group">
            <label for="input6" class="col-sm-2 control-label"><span>بريد الإدارة</span> <span style="color: red">*</span></label>
            <div class="col-sm-10">
                <input type="text" name="6" class="form-control" id="input6" value="{{$settings[6]->value}}" required></input>
            </div>
        </div>
    </div>

    <div class="box-body">
        <div class="form-group">
            <label for="input7" class="col-sm-2 control-label">الهاتف</label>
            <div class="col-sm-10">
                <input type="text" name="7" class="form-control" id="input7" value="{{$settings[7]->value}}"></input>
            </div>
        </div>
    </div>


    <div class="box-body">
        <div class="form-group">
            <label for="input8" class="col-sm-2 control-label">الفاكس</label>
            <div class="col-sm-10">
                <input type="text" name="8" class="form-control" id="input8" value="{{$settings[8]->value}}"></input>
            </div>
        </div>
    </div>


    <div class="box-body">
        <div class="form-group">
            <label for="input9" class="col-sm-2 control-label">العنوان</label>
            <div class="col-sm-10">
                <textarea name="9" class="form-control" id="input9">{{$settings[9]->value}}</textarea>
            </div>
        </div>
    </div>

    <div class="box-body">
        <div class="form-group">
            <div class="col-xs-2">
                <button type="submit" class="btn btn-info btn-block pull-right">حفظ</button>
            </div>
        </div>
    </div>

</form>