<form action="{{route('admin.settings.save_content')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
    {{csrf_field()}}

    <div class="box-body">
        <div class="form-group">
            <label for="input5" class="col-sm-2 control-label">ملخص عن فكرة التسجيل كمسوق</label>
            <div class="col-sm-10">
                <textarea name="5" class="form-control" id="input5"  rows="20" required>{{$settings[5]->value}}</textarea>
            </div>
        </div>
    </div>

    <div class="box-body">
        <div class="form-group">
            <label for="input4" class="col-sm-2 control-label">محتوى الاتفاقية</label>
            <div class="col-sm-10">
                <textarea name="4" class="form-control" id="input4"  rows="20" required>{{$settings[4]->value}}</textarea>
            </div>
        </div>
    </div>

    <div class="box-body">
        <div class="form-group">
            <div class="col-xs-2">
                <button type="submit" class="btn btn-info btn-block pull-right">حفظ</button>
            </div>
        </div>
    </div>

</form>


@section("scripts")
    <script src="https://cdn.ckeditor.com/4.8.0/full/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( '4',{
            language: 'ar',
            uiColor: '#9AB8F3',
            height: 600
        } );
        CKEDITOR.replace( '5',{
            language: 'ar',
            uiColor: '#9AB8F3',
            height: 200
        } );
        CKEDITOR.replace( '10',{
            language: 'ar',
            uiColor: '#9AB8F3',
            height: 600
        } );
    </script>
@endsection