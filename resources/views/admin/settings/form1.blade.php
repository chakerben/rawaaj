<form action="{{route('admin.settings.save_content')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
    {{csrf_field()}}

    <h4>الصفحة الرئيسية</h4>

    <div class="box-body">
        <div class="form-group">
            <label for="input1" class="col-sm-2 control-label">الفقرة الأولى</label>
            <div class="col-sm-10">
                <textarea name="1" class="form-control" id="input1" maxlength="360" rows="4" required>{{$settings[1]->value}}</textarea>
            </div>
        </div>
    </div>

    <div class="box-body">
        <div class="form-group">
            <label for="input2" class="col-sm-2 control-label">الفقرة الأولى</label>
            <div class="col-sm-10">
                <textarea name="2" class="form-control" id="input2" maxlength="300" rows="3" required>{{$settings[2]->value}}</textarea>
            </div>
        </div>
    </div>

    <div class="box-body">
        <div class="form-group">
            <label for="input3" class="col-sm-2 control-label">الفقرة الأولى</label>
            <div class="col-sm-10">
                <textarea name="3" class="form-control" id="input3" maxlength="300" rows="3" required>{{$settings[3]->value}}</textarea>
            </div>
        </div>
    </div>

    <hr>
    <h4>صفحة نبذة عن رواج</h4>


    <div class="box-body">
        <div class="form-group">
            <label for="input10" class="col-sm-2 control-label">محتوى الصفحة</label>
            <div class="col-sm-10">
                <textarea name="10" class="form-control" id="input10"  rows="20" required>{{$settings[10]->value}}</textarea>
            </div>
        </div>
    </div>

    <div class="box-body">
        <div class="form-group">
            <div class="col-xs-2">
                <button type="submit" class="btn btn-info btn-block pull-right">حفظ</button>
            </div>
        </div>
    </div>

</form>