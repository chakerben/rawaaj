@extends('admin.app')

@section('title', 'رسالة زائر')
@section('menu', 8)

@section('content')

    <section class="content-header">
        <h1>
            رسائل الزوار
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
            <li><i class="fa fa-envelope-o"><a href="{{route('contacts.index')}}"></i> رسائل الزوار</li></a>
            <li><i class="fa fa-envelope-o"></i> رسالة زائر</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h5 class="box-title">رسالة زائر</h5>
                <div class="pull-left">
                    <a class="btn bg-teal color-palette" href="{{route('contacts.index')}}"><i
                                class="fa fa-arrow-right"></i> عودة</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="box-body">
                <hr>
                <h5>الإسم :</h5>
                <p>{{@$contact->name}}</p>
                <br>
                <h5>البريد الإلكتروني :</h5>
                <p>{{@$contact->email}}</p>
                <br>
                <h5>الموضوع :</h5>
                <p>{{@$contact->subject}}</p>
                <br>
                <h5>الرسالة :</h5>
                <p>{{@$contact->message}}</p>
                <br>
            </div>
        </div>
    </section>

@endsection