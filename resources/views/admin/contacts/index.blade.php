@extends('admin.app')

@section('title', 'الرسائل')
@section('menu', 8)

@section('content')

    <section class="content-header">
        <h1>
            رسائل الزوار
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
            <li><i class="fa fa-envelope"></i> رسائل الزوار</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">قائمة رسائل الزوار</h3>
            </div>
            <div class="box-body">
                <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th>الإسم</th>
                        <th>البريد الإلكتروني</th>
                        <th>الموضوع</th>
                        <th>التاريخ</th>
                        <th>الحالة</th>
                        <th>الأوامر</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($contacts as $contact)
                        <tr>
                            <td class="text-center">{{$loop->index+1}}</td>
                            <td>{{$contact->name}}</td>
                            <td>{{$contact->email}}</td>
                            <td>{{$contact->subject}}</td>
                            <td class="text-center">{{date("Y/m/d", strtotime($contact->created_at))}}</td>
                            <td class="text-center">
                                @if($contact->read==1)
                                    <a class="btn btn-success btn-xs">مقروء</a>
                                @else
                                    <a class="btn btn-warning btn-xs">غير مقروء</a>
                                @endif
                            </td>
                            <td class="text-center">
                                <a class="btn btn-info btn-xs" href="{{route('contacts.show', $contact->id)}}">
                                    <i class="fa fa-eye"></i> <span>مشاهدة</span>
                                </a>
                                <a class="btn btn-danger btn-xs" href="{{route('contacts.delete', $contact->id)}}"
                                   onclick="return confirm('هل انت متأكد؟')">
                                    <i class="fa fa-trash"></i> <span>حذف</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>

@endsection
