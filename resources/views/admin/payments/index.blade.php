@extends('admin.app')

@section('title', 'التقارير المالية')
@section('menu', 2)

@section('content')

    <section class="content-header">
        <h1>
            التقارير المالية
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
            <li><i class="fa fa-money"></i> التقارير المالية</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">قائمة التصفيات</h3>
                <br><br>
                <strong><span>تاريخ آخر تصفية :</span> <span>@if($last_payement) {{date("Y/m/d", strtotime($last_payement->created_at))}} @else لا يوجد @endif</span></strong>
            </div>
            <div class="box-body">
                <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th>اسم المنتج</th>
                        <th>المشترك</th>
                        <th>المسوق</th>
                        <th>تاريخ الإشتراك</th>
                        <th>تاريخ التصفية</th>
                        <th>نسبة المسوق</th>
                        <th>المبلغ</th>
                        <th>الحالة</th>
                        <th class="text-center">الأوامر</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($payments as $payment)
                        <tr>
                            <td class="text-center">{{$loop->index+1}}</td>
                            <td>{{$payment->product->name}}</td>
                            <td>{{$payment->inscription->name}}</td>
                            <td>{{$payment->user->name}}</td>
                            <td class="text-center">{{date("Y/m/d", strtotime($payment->inscription->created_at))}}</td>
                            <td class="text-center">{{date("Y/m/d", strtotime($payment->created_at))}}</td>
                            <td class="text-center">{{$payment->percentage}} %</td>
                            <td class="text-center">{{$payment->final_price}} رس</td>
                            <td class="text-center">
                                @if($payment->status==1)
                                    <a class="btn btn-success btn-xs">مسدد</a>
                                @else
                                    <a class="btn btn-warning btn-xs">في الإنتظار</a>
                                @endif
                            </td>
                            <td class="text-center">
                                @if($payment->status==0)
                                    <a class="btn btn-primary btn-xs" href="{{route('payments.pay', $payment->id)}}">
                                        <i class="fa fa-money"></i> <span>تسجيل دفع</span>
                                    </a>
                                @endif
                                <a class="btn btn-danger btn-xs" href="{{route('payments.delete', $payment->id)}}" onclick="return confirm('هل أنت متأكد')">
                                    <i class="fa fa-trash"></i> <span>حذف</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tfoot>
                </table>
            </div>
        </div>
    </section>

@endsection
