<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-right image">
                <img src="{{assetAdmin('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> نشط</a>
            </div>
        </div>
        <!-- search form -->
        <form action="{{route('admin.users.index')}}" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="keyword" class="form-control keyword" placeholder="بحث في المسوقين ...">
                <span class="input-group-btn">
                <button type="submit" id="search-btn" class="btn btn-flat"><i
                            class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <script>
            $(function () {
                $(document).on('click', '.sidebar-form button[type=submit]', function(e) {
                   var keyword = $('.sidebar-form .keyword').val();
                   if(keyword==""){
                       alert("يجب كتابة كلمة البحث !");
                       e.preventDefault();
                   }else {
                       var action = $(this).attr("action");
                       $(this).attr(action+"/"+keyword);
                   }
               });
            });
        </script>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">القائمة الرئيسية</li>
            <li class="@if($__env->yieldContent('menu')==1) active @endif">
                <a href="{{route('admin.dashboard')}}">
                    <i class="fa fa-dashboard"></i> <span>لوحة التحكم</span>
                </a>
            </li>

            @if(Auth::user()->role==1 or in_array(2, @$roless))
                <li class="@if($__env->yieldContent('menu')==2) active @endif">
                    <a href="{{route('admin.admins.index')}}">
                        <i class="fa fa-user-secret"></i> <span>المستخدمون</span>
                    </a>
                </li>
            @endif

            @if(Auth::user()->role==1 or in_array(3, @$roless))
                <li class="@if($__env->yieldContent('menu')==3) active @endif">
                    <a href="{{route('admin.users.index')}}">
                        <i class="fa fa-user"></i> <span>المسوقون</span>
                    </a>
                </li>
            @endif

            @if(Auth::user()->role==1 or in_array(4, @$roless))
                <li class="@if($__env->yieldContent('menu')==4) active @endif">
                    <a href="{{route('admin.products.index')}}">
                        <i class="fa fa-shopping-cart"></i> <span>المنتجات</span>
                    </a>
                </li>
            @endif

            @if(Auth::user()->role==1 or in_array(5, @$roless))
                <li class="@if($__env->yieldContent('menu')==5) active @endif">
                    <a href="{{route('dashboard.inscriptions')}}">
                        <i class="fa fa-thumbs-up"></i> <span>الإشتراكات</span>
                    </a>
                </li>
            @endif

            @if(Auth::user()->role==1 or in_array(6, @$roless))
                <li class="@if($__env->yieldContent('menu')==6) active @endif">
                    <a href="{{route('dashboard.visitors')}}">
                        <i class="fa fa-eye"></i> <span>الزيارات</span>
                    </a>
                </li>
            @endif


            @if(Auth::user()->role==1 or in_array(10, @$roless))
                <li class="@if($__env->yieldContent('menu')==10) active @endif">
                    <a href="{{route('payments.index')}}">
                        <i class="fa fa-money"></i> <span>التقارير المالية</span>
                    </a>
                </li>
            @endif

            @if(Auth::user()->role==1 or in_array(7, @$roless) or in_array(8, @$roless))
                <li class="treeview @if($__env->yieldContent('menu')==7 or $__env->yieldContent('menu')==8) active @endif">
                <a href="{{route('dashboard.visitors')}}">
                    <i class="fa fa-envelope"></i> <span>الرسائل</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @if(Auth::user()->role==1 or in_array(7, @$roless))
                        <li class="@if($__env->yieldContent('menu')==7) active @endif">
                            <a href="{{route('messages.index')}}">
                                <i class="fa fa-envelope"></i> <span>رسائل المسوقين</span>
                            </a>
                        </li>
                    @endif
                    @if(Auth::user()->role==1 or in_array(8, @$roless))
                        <li class="@if($__env->yieldContent('menu')==8) active @endif">
                            <a href="{{route('contacts.index')}}">
                                <i class="fa fa-envelope-o"></i> <span>رسائل الزوار</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </li>
            @endif

            @if(Auth::user()->role==1 or in_array(9, @$roless))
                <li class="@if($__env->yieldContent('menu')==9) active @endif">
                    <a href="{{route('admin.settings')}}">
                        <i class="fa fa-gears"></i> <span>الإعدادات</span>
                    </a>
                </li>
            @endif
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>