<header class="main-header">
    <!-- Logo -->
    <a href="{{route('admin.dashboard')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>رواج</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>رواج</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <li class="dropdown notifications-menu">
                    <a href="{{route('home')}}" target="_blank">
                        <i class="fa fa-globe"></i>
                    </a>
                </li>

                <!-- Notifications: style can be found in dropdown.less -->
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope"></i>
                        @if(count(@$msgs)>0)
                            <span class="label label-danger">{{count(@$msgs)}}</span>
                        @endif
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">لديك {{count(@$msgs)}} رسائل مسوقين غير مقروءة</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                @foreach(@$msgs as $msg)
                                    <li>
                                        <a href="{{route('messages.response', $msg->id)}}">
                                            <i class="fa fa-envelope-o text-aqua"></i> {{substr($msg->subject,0,100)}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="footer"><a href="{{route('messages.index')}}">مشاهدة الكل</a></li>
                    </ul>
                </li>

                <!-- Notifications: style can be found in dropdown.less -->
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        @if(count(@$contcts)>0)
                            <span class="label label-warning">{{count(@$contcts)}}</span>
                        @endif
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">لديك {{count(@$contcts)}} رسائل زوار غير مقروءة</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                @foreach(@$contcts as $contacts)
                                    <li>
                                        <a href="{{route('contacts.show', $contacts->id)}}">
                                            <i class="fa fa-envelope-o text-aqua"></i> {{substr($contacts->subject,0,100)}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="footer"><a href="{{route('contacts.index')}}">مشاهدة الكل</a></li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{assetAdmin('dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{Auth::user()->name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{assetAdmin('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                            <p>
                                مرحبا : {{Auth::user()->name}}
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-right">
                                <a href="{{route('admin.admins.edit', Auth::user()->id)}}" class="btn btn-default btn-flat">الملف الشخصي</a>
                            </div>
                            <div class="pull-left">
                                <a href="{{route('admin.auth.logout')}}" class="btn btn-default btn-flat">تسجيل الخروج</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>