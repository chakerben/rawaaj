<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.4 -->
<script src="{{assetAdmin('bootstrap/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{assetAdmin('plugins/fastclick/fastclick.min.js')}}"></script>
<!-- DataTables -->
<script src="{{assetAdmin('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{assetAdmin('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<!-- Select2 -->
<script src="{{assetAdmin('plugins/select2/select2.full.min.js')}}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{assetAdmin('plugins/iCheck/icheck.min.js')}}"></script>

@section('scripts')@show

<!-- AdminLTE App -->
<script src="{{assetAdmin('dist/js/app.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{assetAdmin('dist/js/demo.js')}}"></script>

<script>
    $(function () {
        setTimeout(function () {
            $(".alert").fadeOut("slow")
        }, 3000);
        $(".select2").select2({
            dir: "rtl"
        });
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });
        $('#dataTable,#dataTable2').DataTable({
            language: {
                "sProcessing": "جارٍ التحميل...",
                "sLengthMenu": "أظهر  _MENU_  نتائج",
                "sZeroRecords": "لم يعثر على أية نتائج",
                "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ نتيجة",
                "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 نتيجة",
                "sInfoFiltered": "(منتقاة من مجموع _MAX_ نتائج)",
                "sInfoPostFix": "",
                "sSearch": "بحث : ",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "الأول",
                    "sPrevious": "السابق",
                    "sNext": "التالي",
                    "sLast": "الأخير"
                }
            }
            ,
            "pageLength": 25
        });
    });
</script>