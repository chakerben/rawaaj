@extends('admin.app')

@section('title', 'المسوقون')
@section('menu', 3)

@section('content')

    <section class="content-header">
        <h1>
            المسوقون
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
            <li><i class="fa fa-user"></i> المسوقون</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">قائمة المسوقين</h3>
                <div class="pull-left">
                    <a class="btn bg-purple color-palette" href="{{route('admin.users.create')}}"><i
                                class="fa fa-plus"></i> إضافة</a>
                </div>
                <div class="clearfix"></div>
                @if($keyword)
                    <p>كلمة البحث : <b>{{$keyword}}</b> <br>
                        <small>(<a href="{{route('admin.users.index')}}">إظهار الكل</a>)</small>
                    </p>
                @endif
            </div>
            <div class="box-body">
                <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th class="text-center">الإسم</th>
                        <th class="text-center">تاريخ التسجيل</th>
                        <th class="text-center">تاريخ آخر تسجيل دخول</th>
                        <th class="text-center">عدد الإشتراكات</th>
                        <th class="text-center">عدد الزياات</th>
                        <th class="text-center">النسبة</th>
                        <th class="text-center">الحالة</th>
                        <th class="text-center" width="21%">الأوامر</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td class="text-center">{{$loop->index+1}}</td>
                            <td>{{$user->name}}</td>
                            <td class="text-center">{{date("Y/m/d", strtotime($user->created_at))}}</td>
                            <td class="text-center">{{($user->last_login)?date("Y/m/d", strtotime($user->last_login)):""}}</td>
                            <td class="text-center">{{$user->num_inscriptions()}}</td>
                            <td class="text-center">{{$user->num_visits()}}</td>
                            <td class="text-center">
                                @if($user->percentage)
                                    <span>{{$user->percentage}} %</span>
                                @else
                                    <span>لا يوجد</span>
                                @endif
                            </td>
                            <td class="text-center">
                                @if($user->status==1)
                                    <a class="btn btn-success btn-xs">مفعل</a>
                                @else
                                    <a class="btn btn-warning btn-xs">غير مفعل</a>
                                @endif
                            </td>
                            <td class="text-center">
                                <a class="btn btn-warning btn-xs" href="{{route('admin.users.payments', $user->id)}}">
                                    <i class="fa fa-money"></i> <span>إستعراض</span>
                                </a>
                                <a class="btn btn-primary btn-xs" href="{{route('admin.users.edit', $user->id)}}">
                                    <i class="fa fa-edit"></i> <span>تعديل</span>
                                </a>
                                <a class="btn btn-danger btn-xs" href="{{route('admin.users.delete', $user->id)}}"
                                   onclick="return confirm('هل انت متأكد؟')">
                                    <i class="fa fa-trash"></i> <span>حذف</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tfoot>
                </table>
            </div>
        </div>
    </section>

@endsection