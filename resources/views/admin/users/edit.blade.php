@extends('admin.app')

@section('title', 'تعديل مسوق')
@section('menu', 3)

@section('content')

    <section class="content-header">
        <h1>
            المستخدمون
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
            <li><a href="{{route('admin.users.index')}}"><i class="fa fa-user"></i> المسوقون</a></li>
            <li><i class="fa fa-edit"></i> تعديل مسوق</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">تعديل مسوق</h3>
                <div class="pull-left">
                    <a class="btn bg-teal color-palette" href="{{route('admin.users.index')}}"><i
                                class="fa fa-arrow-right"></i> عودة</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="box-body">
                <div class="col-md-6 col-sm-offset-3">
                    <form action="{{route('admin.users.update', $user->id)}}" method="post" class="form-horizontal">
                        {{csrf_field()}}

                        <div class="box-body">
                            <div class="form-group">
                                <label for="code" class="col-sm-4 control-label">كود المسوق</label>
                                <div class="col-sm-8">
                                    <input type="text" readonly class="form-control" id="code" value="{{$user->code}}" style="color: #027a98;width: 130px;">
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="name" class="col-sm-4 control-label">الإسم</label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" class="form-control" id="name" value="{{$user->name}}" placeholder="الإسم" required>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="email" class="col-sm-4 control-label">البريد الإلكتروني</label>
                                <div class="col-sm-8">
                                    <input type="email" name="email" class="form-control" id="email"
                                           placeholder="البريد الإلكتروني" value="{{$user->email}}" required>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="phone" class="col-sm-4 control-label">الهاتف</label>
                                <div class="col-sm-8">
                                    <input type="tel" name="phone" class="form-control" id="phone" placeholder="الهاتف" value="{{$user->phone}}" maxlength="20">
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="password" class="col-sm-4 control-label">كلمة المرور</label>
                                <div class="col-sm-8">
                                    <input type="password" name="password" class="form-control" id="password"
                                           placeholder="كلمة المرور">
                                    <small>إذا لم تكن تريد تعديل كلمة المرور أترك هذا الحقل فارغا</small>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="status" class="col-sm-4 control-label">الحالة</label>
                                <div class="col-sm-8">
                                    <label>
                                        <input type="radio" name="status" value="1" id="status" class="minimal" @if($user->status==1) checked @endif> <span> مفعل</span>
                                    </label>
                                    <label>
                                        <input type="radio" name="status" value="0" class="minimal" @if($user->status==0) checked @endif> <span> غير مفعل</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="percentage" class="col-sm-4 control-label">نسبة المسوق</label>
                                <div class="col-sm-8">
                                    <input type="number" name="percentage" class="form-control" id="percentage" value="{{$user->percentage}}" min="0", max="100" step="0.01">
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">

                                <div class="col-sm-4">
                                    <button type="submit" class="btn btn-info btn-block pull-right">تعديل</button>
                                </div>
                            </div>
                        </div>

                    </form>

                    <div class="box-body">
                        <hr>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="code" class="col-sm-4 control-label">رقم الحساب البنكي</label>
                            <div class="col-sm-8">
                                {{$user->account}}
                            </div>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="code" class="col-sm-4 control-label">العمر</label>
                            <div class="col-sm-8">
                                {{$user->age}}
                            </div>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="code" class="col-sm-4 control-label">الجنس</label>
                            <div class="col-sm-8">
                                @if($user->gender==1)
                                    <span>ذكر</span>
                                @elseif($user->gender==2)
                                    <span>أنثى</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="code" class="col-sm-4 control-label">المدينة</label>
                            <div class="col-sm-8">
                                {{$user->city}}
                            </div>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="code" class="col-sm-4 control-label">التفرغ للعمل كمسوق</label>
                            <div class="col-sm-8">
                                {{$user->availability}}
                            </div>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="code" class="col-sm-4 control-label">المجالات والاهتمامات</label>
                            <div class="col-sm-8">
                                {{$user->interests}}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection