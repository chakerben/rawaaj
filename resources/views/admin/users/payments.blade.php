@extends('admin.app')

@section('title', 'إستعراض البيانات')
@section('menu', 3)

@section('content')

    <section class="content-header">
        <h1>
            المسوقون
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
            <li><a href="{{route('admin.users.index')}}"><i class="fa fa-user"></i> المسوقون</a></li>
            <li><i class="fa fa-money"></i> إستعراض البيانات</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">إستعراض بيانات المسوق <span>"{{$user->name}}"</span></h3>
                <div class="pull-left">
                    <a class="btn bg-teal color-palette" href="{{route('admin.users.index')}}"><i
                                class="fa fa-arrow-right"></i> عودة</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="box-body">

                <div class="panel panel-primary">
                    <div class="panel-heading"><h5>الإشتراكات</h5></div>
                    <div class="panel-body">
                        <table id="dataTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-center" width="3%"></th>
                                <th class="text-center">المنتج</th>
                                <th class="text-center">المشترك</th>
                                <th class="text-center">الحالة</th>
                                <th class="text-center">تاريخ الإشتراك</th>
                                <th class="text-center">الأوامر</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($inscriptions as $inscription)
                                <tr>
                                    <td class="text-center">{{$loop->index+1}}</td>
                                    <td>{{$inscription->product->name}}</td>
                                    <td>{{$inscription->name}}<br>{{$inscription->email}}</td>
                                    <td class="text-center" style="border-left: 1px solid #dddddd;">
                                        @if($inscription->status==1)
                                            <a class="btn btn-success btn-xs">مقبول</a>
                                        @else
                                            <a class="btn btn-warning btn-xs">في الإنتظار</a>
                                        @endif
                                    </td>
                                    <td class="text-center">{{date('Y/m/d', strtotime($inscription->created_at))}}</td>
                                    <td class="text-center">
                                        @if($inscription->status==0)
                                            <a class="btn btn-info btn-xs"
                                               href="{{route('dashboard.inscription_status',["id"=>$inscription->id, "status"=>1])}}"><i
                                                        class="fa fa-thumbs-up"></i> قبول</a>
                                        @endif
                                        <a class="btn btn-danger btn-xs" onclick="return confirm('هل أنت متأكد؟')"
                                           href="{{route('dashboard.inscription_status',["id"=>$inscription->id, "status"=>2])}}"><i
                                                    class="fa fa-thumbs-down"></i> حذف</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading"><h5>التصفيات</h5></div>
                    <div class="panel-body">
                        <?php $totall = 0;?>
                        <table id="dataTable2" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th></th>
                                <th>اسم المنتج</th>
                                <th>المشترك</th>
                                <th>تاريخ الإشتراك</th>
                                <th>تاريخ التصفية</th>
                                <th>نسبة المسوق</th>
                                <th>المبلغ</th>
                                <th>الحالة</th>
                                <th class="text-center">الأوامر</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payments as $payment)
                                <tr>
                                    <td class="text-center">{{$loop->index+1}}</td>
                                    <td>{{$payment->product->name}}</td>
                                    <td>{{$payment->inscription->name}}</td>
                                    <td class="text-center">{{date("Y/m/d", strtotime($payment->inscription->created_at))}}</td>
                                    <td class="text-center">{{date("Y/m/d", strtotime($payment->created_at))}}</td>
                                    <td class="text-center">{{$payment->percentage}} %</td>
                                    <td class="text-center">{{$payment->final_price}} رس</td>
                                    <td class="text-center">
                                        @if($payment->status==1)
                                            <a class="btn btn-success btn-xs">مسدد</a>
                                        @else
                                            <a class="btn btn-warning btn-xs">في الإنتظار</a>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if($payment->status==0)
                                            <a class="btn btn-primary btn-xs"
                                               href="{{route('payments.pay', $payment->id)}}">
                                                <i class="fa fa-money"></i> <span>تسجيل دفع</span>
                                            </a>
                                        @endif
                                        <a class="btn btn-danger btn-xs"
                                           href="{{route('payments.delete', $payment->id)}}"
                                           onclick="return confirm('هل أنت متأكد')">
                                            <i class="fa fa-trash"></i> <span>حذف</span>
                                        </a>
                                    </td>
                                </tr>
                                @if($payment->status==0)
                                    <?php $totall+=$payment->final_price; ?>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                        <br>
                        <strong><span>تاريخ آخر تصفية :</span>
                            <span>@if($last_payement) {{date("Y/m/d", strtotime($last_payement->created_at))}} @else لا
                                يوجد @endif</span></strong><br>
                        <strong><span>المبلغ المستحق :</span> <span>{{$totall}}</span> رس</strong>
                        <br>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection