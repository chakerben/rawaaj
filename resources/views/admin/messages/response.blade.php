@extends('admin.app')

@section('title', 'الرد على رسالة')
@section('menu', 7)

@section('content')

    <section class="content-header">
        <h1>
            رسائل المسوقين
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
            <li><i class="fa fa-envelope"></i> رسائل المسوقين</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">الرد على رسالة</h3>
                <div class="pull-left">
                    <a class="btn bg-teal color-palette" href="{{route('messages.index')}}"><i
                                class="fa fa-arrow-right"></i> عودة</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="box-body">
                <div class="col-md-10 col-sm-offset-1">
                    <form action="{{route('messages.send_response', $message->id)}}" method="post"
                          class="form-horizontal" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <h5><span>الرسالة</span> <i class="bg-gray color-palette"
                                                    style="padding: 0px 10px;margin-right: 10px;">{{date("G:i Y/m/d", strtotime($message->created_at))}}</i>
                        </h5>
                        <p style="text-align: justify">{{$message->message}}</p>
                        @if($message->file)
                            <a target="_blank" href="{{asset("public/uploads/messages/".$message->file)}}"><i class="fa fa-link"></i> ملف مرفق</a>
                        @endif
                        <hr>

                        @if(count($message->messages)>0)
                            <h4 style="margin-bottom: 20px;">الردود : </h4>
                            @foreach($message->messages as $m)
                                <h5>
                                    @if($m->response_from==0)
                                        <span>الإدارة</span>
                                    @else
                                        <span>{{$message->user->name}}</span>
                                    @endif
                                    <i class="bg-gray color-palette"
                                       style="padding: 0px 10px;margin-right: 10px;">{{date("G:i Y/m/d", strtotime($m->created_at))}}</i>
                                </h5>
                                <p style="text-align: justify">{{$m->message}}</p>
                                @if($m->file)
                                    <a target="_blank" href="{{asset("public/uploads/messages/".$m->file)}}"><i class="fa fa-link"></i> ملف مرفق</a>
                                @endif
                                <hr>
                            @endforeach
                        @endif


                        <div class="box-body">
                            <div class="form-group">
                                <label for="response" class="col-sm-1 control-label">الرد</label>
                                <div class="col-sm-11">
                                    <textarea required name="response" class="form-control" id="response"
                                              rows="6"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="file" class="col-sm-1 control-label">ملف مرفق</label>
                                <div class="col-sm-11">
                                    <input type="file" name="file" class="form-control" id="file">
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <div class="col-sm-2 col-sm-offset-1">
                                    <button type="submit" class="btn btn-info btn-block pull-right">إرسال</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection