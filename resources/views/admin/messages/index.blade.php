@extends('admin.app')

@section('title', 'الرسائل')
@section('menu', 7)

@section('content')

    <section class="content-header">
        <h1>
            رسائل المسوقين
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
            <li><i class="fa fa-envelope"></i> رسائل المسوقين</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">قائمة رسائل المسوقين</h3>
            </div>
            <div class="box-body">
                <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th>المسوق</th>
                        <th>الموضوع</th>
                        <th>التاريخ</th>
                        <th>التوقيت</th>
                        <th>الحالة</th>
                        <th>ملف مرفق</th>
                        <th>الأوامر</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($messages as $message)
                        <tr>
                            <td class="text-center">{{$loop->index+1}}</td>
                            <td><a href="{{route('admin.users.edit', $message->user->id)}}">{{$message->user->name}}</a></td>
                            <td>{{$message->subject}}</td>
                            <td>{{date("Y/m/d", strtotime($message->created_at))}}</td>
                            <td>{{date("G:i:s", strtotime($message->created_at))}}</td>
                            <td class="text-center">
                                @if($message->status==1)
                                    <a class="btn btn-success btn-xs">مقروء</a>
                                @else
                                    <a class="btn btn-warning btn-xs">غير مقروء</a>
                                @endif
                            </td>
                            <td class="text-center" >
                                @if($message->file)
                                    <a class="brn btn-danger btn-xs" target="_blank" href="{{asset("public/uploads/messages/".$message->file)}}"><i class="fa fa-download"></i> تنزيل</a>
                                @else
                                    <span>لا يوجد</span>
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-primary btn-xs" href="{{route('messages.response', $message->id)}}">
                                    <i class="fa fa-edit"></i> <span>رد</span>
                                </a>
                                <a class="btn btn-danger btn-xs" href="{{route('messages.delete', $message->id)}}"
                                   onclick="return confirm('هل انت متأكد؟')">
                                    <i class="fa fa-trash"></i> <span>حذف</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>

@endsection