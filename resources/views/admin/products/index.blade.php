@extends('admin.app')

@section('title', 'المنتجات')
@section('menu', 4)

@section('content')

    <section class="content-header">
        <h1>
            المنتجات
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
            <li><i class="fa fa-shopping-cart"></i> المنتجات</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">قائمة المنتجات</h3>
                <div class="pull-left">
                    <a class="btn bg-purple color-palette" href="{{route('admin.products.create')}}"><i
                                class="fa fa-plus"></i> إضافة</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="box-body">
                <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th>الصورة</th>
                        <th>الكود</th>
                        <th>الإسم</th>
                        <th>الثمن</th>
                        <th>نسبة التسويق</th>
                        <th>متاح للتسويق</th>
                        <th>الحالة</th>
                        <th width="100px">الأوامر</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td class="text-center">{{$loop->index+1}}</td>
                            <td><img src="{{asset('public/uploads/images/'.$product->image)}}" width="100px"></td>
                            <td>{{$product->ref}}</td>
                            <td>{{$product->name}}</td>
                            <td class="text-center">{{$product->price}} <span>ريال</span></td>
                            <td class="text-center">{{$product->percentage}} %</td>
                            <td class="text-center">
                                @if($product->status==1)
                                    <a class="btn btn-success btn-xs">متاح</a>
                                @else
                                    <a class="btn btn-warning btn-xs">غير متاح</a>
                                @endif
                            </td>
                            <td class="text-center">
                                @if($product->published==1)
                                    <a class="btn btn-success btn-xs">منشور</a>
                                @else
                                    <a class="btn btn-warning btn-xs">غير منشور</a>
                                @endif
                            </td>
                            <td class="text-center">
                                <a class="btn btn-primary btn-xs" href="{{route('admin.products.edit', $product->id)}}">
                                    <i class="fa fa-edit"></i> <span>تعديل</span>
                                </a>
                                <a class="btn btn-danger btn-xs" href="{{route('admin.products.delete', $product->id)}}"
                                   onclick="return confirm('هل انت متأكد؟')">
                                    <i class="fa fa-trash"></i> <span>حذف</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tfoot>
                </table>
            </div>
        </div>
    </section>

@endsection