@extends('admin.app')

@section('title', 'إضافة منتج')
@section('menu', 4)

@section('content')

    <section class="content-header">
        <h1>
            المنتجات
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
            <li><a href="{{route('admin.products.index')}}"><i class="fa fa-shopping-cart"></i> المنتجات</a></li>
            <li><i class="fa fa-plus"></i> إضافة منتج</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">إضافة منتج</h3>
                <div class="pull-left">
                    <a class="btn bg-teal color-palette" href="{{route('admin.products.index')}}"><i
                                class="fa fa-arrow-right"></i> عودة</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="box-body">
                <div class="col-md-10 col-sm-offset-1">
                    <form action="{{route('admin.products.store')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="box-body">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">الإسم</label>
                                <div class="col-sm-9">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="الإسم" required>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="short_description" class="col-sm-3 control-label">النبذة</label>
                                <div class="col-sm-9">
                                    <textarea name="short_description" class="form-control" id="short_description" placeholder="النبذة" rows="2" required maxlength="200"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="description" class="col-sm-3 control-label">الوصف</label>
                                <div class="col-sm-9">
                                    <textarea name="description" class="form-control" id="description" placeholder="الوصف" rows="6" required></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="image" class="col-sm-3 control-label">الصورة</label>
                                <div class="col-sm-9">
                                    <input type="file" name="image" accept="image/*" class="form-control" id="name" required>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="url" class="col-sm-3 control-label"> الرابط الأصلى</label>
                                <div class="col-sm-9">
                                    <input type="url" name="url" class="form-control" id="url" placeholder="الرابط" required>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="percentage" class="col-sm-3 control-label">نسبة التسويق</label>
                                <div class="col-sm-9">
                                    <input type="number" name="percentage" class="form-control" id="percentage" min="0" max="100" step="0.01" required style="width: 100px">
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="price" class="col-sm-3 control-label">الثمن</label>
                                <div class="col-sm-9">
                                    <input type="number" name="price" class="form-control" id="price" placeholder="الثمن" required style="width: 100px">
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="status" class="col-sm-3 control-label">متاح للتسويق</label>
                                <div class="col-sm-9">
                                    <label>
                                        <input type="radio" name="status" value="1" id="status" class="minimal" checked> <span> متاح</span>
                                    </label>
                                    <label>
                                        <input type="radio" name="status" value="0" class="minimal"> <span> غير متاح</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="published" class="col-sm-3 control-label">الحالة</label>
                                <div class="col-sm-9">
                                    <label>
                                        <input type="radio" name="published" value="1" id="published" class="minimal" checked> <span> منشور</span>
                                    </label>
                                    <label>
                                        <input type="radio" name="published" value="0" class="minimal"> <span> غير منشور</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <div class="col-xs-3">
                                    <button type="submit" class="btn btn-info btn-block pull-right">حفظ</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
<script src="https://cdn.ckeditor.com/4.8.0/standard-all/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'description',{
        language: 'ar',
        uiColor: '#9AB8F3'
    } );
</script>
@endsection