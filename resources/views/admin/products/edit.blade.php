@extends('admin.app')

@section('title', 'تعديل منتج')
@section('menu', 4)

@section('content')

    <section class="content-header">
        <h1>
            المنتجات
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
            <li><a href="{{route('admin.products.index')}}"><i class="fa fa-shopping-cart"></i> المنتجات</a></li>
            <li><i class="fa fa-plus"></i> تعديل منتج</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">تعديل منتج</h3>
                <div class="pull-left">
                    <a class="btn bg-teal color-palette" href="{{route('admin.products.index')}}"><i
                                class="fa fa-arrow-right"></i> عودة</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="box-body">
                <div class="col-md-10 col-sm-offset-1">
                    <form action="{{route('admin.products.update', $product->id)}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="box-body">
                            <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">كود المنتج</label>
                                <div class="col-sm-9">
                                    <input type="text" readonly class="form-control" id="code" value="{{$product->ref}}" style="color: #027a98;width: 130px;">
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">الإسم</label>
                                <div class="col-sm-9">
                                    <input type="text" name="name" value="{{$product->name}}" class="form-control" id="name" placeholder="الإسم" required>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="short_description" class="col-sm-3 control-label">النبذة</label>
                                <div class="col-sm-9">
                                    <textarea name="short_description" class="form-control" id="short_description" placeholder="النبذة" rows="2" required maxlength="200">{!! $product->short_description !!}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="description" class="col-sm-3 control-label">الوصف</label>
                                <div class="col-sm-9">
                                    <textarea name="description" class="form-control" id="description" placeholder="الوصف" rows="6" required>{!! $product->description !!}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="image" class="col-sm-3 control-label">الصورة</label>
                                <div class="col-sm-9">
                                    <input type="file" name="image" accept="image/*" class="form-control" id="name" >
                                    <br>
                                    <img src="{{asset('public/uploads/images/'.$product->image)}}" width="200px">
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="url" class="col-sm-3 control-label">الرابط الأصلي</label>
                                <div class="col-sm-9">
                                    <input type="url" name="url" value="{{$product->url}}" class="form-control" id="url" placeholder="الرابط" required>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="percentage" class="col-sm-3 control-label">نسبة التسويق</label>
                                <div class="col-sm-9">
                                    <input type="number" name="percentage" value="{{$product->percentage}}" class="form-control" id="percentage" min="0" max="100" step="0.01" required style="width: 100px">
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="price" class="col-sm-3 control-label">الثمن</label>
                                <div class="col-sm-9">
                                    <input type="number" name="price" value="{{$product->price}}" class="form-control" id="price" placeholder="الثمن" required style="width: 100px">
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="status" class="col-sm-3 control-label">متاح للتسويق</label>
                                <div class="col-sm-9">
                                    <label>
                                        <input type="radio" name="status" value="1" id="status" class="minimal" @if($product->status==1) checked @endif> <span> متاح</span>
                                    </label>
                                    <label>
                                        <input type="radio" name="status" value="0" class="minimal" @if($product->status==0) checked @endif> <span> غير متاح</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="published" class="col-sm-3 control-label">الحالة</label>
                                <div class="col-sm-9">
                                    <label>
                                        <input type="radio" name="published" value="1" id="published" class="minimal" @if($product->published==1) checked @endif> <span> منشور</span>
                                    </label>
                                    <label>
                                        <input type="radio" name="published" value="0" class="minimal" @if($product->published==0) checked @endif> <span> غير منشور</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <div class="col-xs-2">
                                    <button type="submit" class="btn btn-info btn-block pull-right">حفظ</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script src="https://cdn.ckeditor.com/4.8.0/standard-all/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'description',{
            language: 'ar',
            uiColor: '#9AB8F3'
        } );
    </script>
@endsection