@if(count($errors)>0)
    @foreach($errors->all() as $error)
        <div class="alert alert-danger text-right" style="direction: rtl">{{$error}}</div>
    @endforeach
@endif

@if(session()->has('error'))
    <div class="alert alert-danger text-right" style="direction: rtl">{{session('error')}}</div>
@endif

@if(session()->has('success'))
    <div class="alert alert-success text-right" style="direction: rtl">{{session('success')}}</div>
@endif

@if(session()->has('status'))
    <div class="alert alert-success text-right" style="direction: rtl">{{session('status')}}</div>
@endif

