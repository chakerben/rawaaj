@extends('admin.app')

@section('title', 'لوحة التحكم')
@section('menu', 1)

@section('styles')
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{assetAdmin('plugins/morris/morris.css')}}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{assetAdmin('plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{assetAdmin('plugins/datepicker/datepicker3.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{assetAdmin('plugins/daterangepicker/daterangepicker-bs3.css')}}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{assetAdmin('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
@endsection

@section('content')

    <section class="content-header">
        <h1>
            لوحة التحكم
        </h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> لوحة التحكم</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{@$users}}</h3>
                        <p>المسوقون</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person"></i>
                    </div>
                    <a href="{{route('admin.users.index')}}" class="small-box-footer">المزيد <i class="fa fa-arrow-circle-left"></i></a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{@$products}}</h3>
                        <p>المنتجات</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-android-cart"></i>
                    </div>
                    <a href="{{route('admin.products.index')}}" class="small-box-footer">المزيد <i class="fa fa-arrow-circle-left"></i></a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{@$inscriptions}}</h3>
                        <p>الإشتراكات</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-thumbsup"></i>
                    </div>
                    <a href="{{route('dashboard.inscriptions')}}" class="small-box-footer">المزيد <i class="fa fa-arrow-circle-left"></i></a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{@$visitors}}</h3>
                        <p>الزيارات</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-eye"></i>
                    </div>
                    <a href="{{route('dashboard.visitors')}}" class="small-box-footer">المزيد <i class="fa fa-arrow-circle-left"></i></a>
                </div>
            </div><!-- ./col -->
        </div>

        <div class="row">

            <section class="col-lg-5 connectedSortable">

                <div class="box box-solid bg-light-blue-gradient">
                    <div class="box-header">
                        <div class="pull-left box-tools">
                            <button class="btn btn-primary btn-sm" data-widget="collapse"
                                    data-toggle="tooltip" title="Collapse" style="margin-right: 5px;"><i
                                        class="fa fa-minus"></i></button>
                            <button class="btn bg-primary btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                        <i class="fa fa-shopping-cart"></i>
                        <h3 class="box-title">
                            الإشتراكات
                        </h3>
                    </div>
                    <div class="box-body">
                        <div id="world-map" style="height: 250px; width: 100%;"></div>
                    </div>
                </div>

                <!-- solid sales graph -->
                <div class="box box-solid bg-teal-gradient">
                    <div class="box-header">
                        <i class="fa fa-eye"></i>
                        <h3 class="box-title">الزيارات</h3>
                        <div class="box-tools pull-left">
                            <button class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body border-radius-none">
                        <div id="world-map2" style="height: 250px; width: 100%;"></div>
                    </div>
                </div>

            </section>

            <section class="col-lg-7 connectedSortable">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">أفضل العملاء</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body chart-responsive">
                        <div class="chart" id="bar-chart" style="height: 300px;"></div>
                    </div><!-- /.box-body -->
                </div>
            </section>

        </div>

    </section>

@endsection

@section('scripts')
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="{{assetAdmin('plugins/morris/morris.min.js')}}"></script>
    <!-- Sparkline -->
    <script src="{{assetAdmin('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
    <!-- jvectormap -->
    <script src="{{assetAdmin('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{assetAdmin('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{assetAdmin('plugins/knob/jquery.knob.js')}}"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="{{assetAdmin('plugins/daterangepicker/daterangepicker.js')}}"></script>
    <!-- datepicker -->
    <script src="{{assetAdmin('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{assetAdmin('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <!-- Slimscroll -->
    <script src="{{assetAdmin('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{assetAdmin('dist/js/pages/dashboard.js')}}"></script>

    <script>
        $(function () {
            var inscData = {
                @foreach($insc as $item)
                    "{{$item->country_code}}" : {{$item->total}},
                @endforeach
            };
            $('#world-map').vectorMap({
                map: 'world_mill_en',
                backgroundColor: "transparent",
                regionStyle: {
                    initial: {
                        fill: '#e4e4e4',
                        "fill-opacity": 1,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 1
                    }
                },
                series: {
                    regions: [{
                        values: inscData,
                        scale: ["#1d77af", "#0d5a9c"],
                        normalizeFunction: 'polynomial'
                    }]
                },
                onRegionLabelShow: function (e, el, code) {
                    if (typeof inscData[code] != "undefined")
                        el.html(el.html() + '<br><center>' + inscData[code] + ' إشتراكات'+'</center>');
                }
            });
            var inscData2 = {
                @foreach($insc2 as $item)
                "{{$item->country_code}}" : {{$item->total}},
                @endforeach
            };
            $('#world-map2').vectorMap({
                map: 'world_mill_en',
                backgroundColor: "transparent",
                regionStyle: {
                    initial: {
                        fill: '#e4e4e4',
                        "fill-opacity": 1,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 1
                    }
                },
                series: {
                    regions: [{
                        values: inscData2,
                        scale: ["#00bcb9", "#00b3b1"],
                        normalizeFunction: 'polynomial'
                    }]
                },
                onRegionLabelShow: function (e, el, code) {
                    if (typeof inscData2[code] != "undefined")
                        el.html(el.html() + '<br><center>' + inscData2[code] + ' زيارات'+'</center>');
                }
            });
            var bar = new Morris.Bar({
                element: 'bar-chart',
                resize: true,
                data: [
                    @foreach($best as $item)
                        {y: '{{$item->user_id}}', a: {{$item->total}} },
                @endforeach

                ],
                barColors: ['#00a65a', '#f56954'],
                xkey: 'y',
                ykeys: ['a'],
                labels: ['الإشتراكات'],
                hideHover: 'auto'
            });
        });
    </script>
@endsection