<!DOCTYPE html>
<html>
@include('admin.includes.head')
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    @include('admin.includes.topbar')

    @include('admin.includes.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('admin.messages')

        @section('content')@show
        
    </div>
    <!-- /.content-wrapper -->


    @include('admin.includes.footer')

    @include('admin.includes.cotrolSidebar')

</div>
<!-- ./wrapper -->

@include('admin.includes.foot')

</body>
</html>
