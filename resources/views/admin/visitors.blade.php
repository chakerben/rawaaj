@extends('admin.app')

@section('title', 'الزيارات')
@section('menu', 6)

@section('content')

    <section class="content-header">
        <h1>
            الزيارات
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
            <li><i class="fa fa-eye"></i> الزيارات</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">قائمة الزيارات</h3>
            </div>
            <div class="box-body">

                <form class="form-inline filter" action="{{route('dashboard.visitors_filter')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="user_id">المسوق : </label>
                        <select name="user_id" class="form-control">
                            <option value="">الكل</option>
                            @foreach($users as $user)
                                <option value="{{$user->id}}"
                                        @if(@$user_id==$user->id) selected @endif>{{$user->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="product_id">المنتج : </label>
                        <select name="product_id" class="form-control">
                            <option value="">الكل</option>
                            @foreach($products as $product)
                                <option value="{{$product->id}}"
                                        @if(@$product_id==$product->id) selected @endif>{{$product->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="created_at">التاريخ : </label>
                        <link rel="stylesheet"
                              href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css"/>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.ar.min.js"></script>
                        <input type="text" name="created_at" class="form-control datepicker" value="{{@$created_at}}">
                    </div>
                    <button type="submit" class="btn btn-primary">فلتر</button>
                    @if(@$user_id or @$product_id or @$created_at)
                        <a class="btn btn-success" href="{{route('dashboard.visitors')}}">عودة</a>
                    @endif
                </form>
                <div class="clearfix"></div>

                <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th width="3%"></th>
                        <th>المسوق</th>
                        <th>المنتج</th>
                        <th>الدولة</th>
                        <th>المدينة</th>
                        <th>الآي بي</th>
                        <th style="border-left: 1px solid #dddddd;">التاريخ</th>
                        <th>الوقت</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($visitors as $visitor)
                        <tr>
                            <td class="text-center">{{$loop->index+1}}</td>
                            <td><a href="{{route('admin.users.edit',$visitor->user->id)}}">{{$visitor->user->name}}</a></td>
                            <td>{{$visitor->product->name}}</td>
                            <td>
                                <img src="http://www.geonames.org/flags/x/{{strtolower($visitor->country_code)}}.gif" style="width: 20px;padding-top: 4px;">
                                {{$visitor->country}}
                            </td>
                            <td>{{$visitor->city}}</td>
                            <td class="text-center">{{$visitor->ip}}</td>
                            <td class="text-center" style="border-left: 1px solid #dddddd;">{{date('Y/m/d', strtotime($visitor->created_at))}}</td>
                            <td class="text-center">{{date('G:i:s', strtotime($visitor->created_at))}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </section>

    <style>
        .datepicker {
            text-align: right;
            border-radius: 0px;
        }

        select.form-control {
            padding: 2px 12px;
        }

        .filter {
            margin-bottom: 30px;
        }

        .filter button, .filter .btn {
            width: 100px;
        }

        .filter .form-control {
            margin-left: 20px;
        }
    </style>

    <script>
        $(function () {
            $('.datepicker').datepicker({
                language: 'ar',
                format: 'yyyy/mm/dd'
            });
        });
    </script>

    @endsection