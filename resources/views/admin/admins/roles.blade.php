@extends('admin.app')

@section('title', 'صلاحيات الموظفين')
@section('menu', 2)

@section('content')

    <section class="content-header">
        <h1>
            صلاحيات الموظفين
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
            <li><a href="{{route('admin.admins.index')}}"><i class="fa fa-user-secret"></i> المستخدمون</a></li>
            <li><i class="fa fa-gears"></i> صلاحيات الموظفين</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">تعديل صلاحيات الموظفين</h3>
                <div class="pull-left">
                    <a class="btn bg-teal color-palette" href="{{route('admin.admins.index')}}"><i class="fa fa-arrow-right"></i> العودة</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="box-body">
                <div class="col-md-6 col-sm-offset-3">
                    <form action="{{route('admin.admins.save_roles')}}" method="post" class="form-horizontal">
                        {{csrf_field()}}

                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-3 control-label"></label>
                                <div class="col-sm-9 labels">
                                    <label>
                                        <input type="checkbox" name="roles[]" value="2" @if(in_array(2, $roles)) checked @endif id="status" class="minimal"> <span>المستخدمون</span>
                                    </label>
                                    <label>
                                        <input type="checkbox" name="roles[]" value="3" @if(in_array(3, $roles)) checked @endif id="status" class="minimal"> <span>المسوقون</span>
                                    </label>
                                    <label>
                                        <input type="checkbox" name="roles[]" value="4" @if(in_array(4, $roles)) checked @endif id="status" class="minimal"> <span>المنتجات</span>
                                    </label>
                                    <label>
                                        <input type="checkbox" name="roles[]" value="5" @if(in_array(5, $roles)) checked @endif id="status" class="minimal"> <span>الإشتراكات</span>
                                    </label>
                                    <label>
                                        <input type="checkbox" name="roles[]" value="6" @if(in_array(6, $roles)) checked @endif id="status" class="minimal"> <span>الزيارات</span>
                                    </label>
                                    <label>
                                        <input type="checkbox" name="roles[]" value="10" @if(in_array(10, $roles)) checked @endif id="status" class="minimal"> <span>التقارير المالية</span>
                                    </label>
                                    <label>
                                        <input type="checkbox" name="roles[]" value="7" @if(in_array(7, $roles)) checked @endif id="status" class="minimal"> <span>رسائل المسوقين</span>
                                    </label>
                                    <label>
                                        <input type="checkbox" name="roles[]" value="8" @if(in_array(8, $roles)) checked @endif id="status" class="minimal"> <span>رسائل الزوار</span>
                                    </label>
                                    <label>
                                        <input type="checkbox" name="roles[]" value="9" @if(in_array(9, $roles)) checked @endif id="status" class="minimal"> <span>الإعدادات</span>
                                    </label>

                                    <br>
                                    <button type="submit" class="btn btn-info" style="padding: 6px 35px;margin-top: 10px;">حفظ</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>

    <style>
        .labels label{
            display: block;
            margin-bottom: 10px;
        }
    </style>

    @endsection