@extends('admin.app')

@section('title', 'إضافة مستخدم')
@section('menu', 2)

@section('content')

    <section class="content-header">
        <h1>
            المستخدمون
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
            <li><a href="{{route('admin.admins.index')}}"><i class="fa fa-user-secret"></i> المستخدمون</a></li>
            <li><i class="fa fa-plus"></i> إضافة مستخدم</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">إضافة مستخدم</h3>
                <div class="pull-left">
                    <a class="btn bg-teal color-palette" href="{{route('admin.admins.index')}}"><i
                                class="fa fa-arrow-right"></i> عودة</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="box-body">
                <div class="col-md-6 col-sm-offset-3">
                    <form action="{{route('admin.admins.store')}}" method="post" class="form-horizontal">
                        {{csrf_field()}}

                        <div class="box-body">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">الإسم</label>
                                <div class="col-sm-9">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="الإسم" required>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">البريد الإلكتروني</label>
                                <div class="col-sm-9">
                                    <input type="email" name="email" class="form-control" id="email"
                                           placeholder="البريد الإلكتروني" required>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="password" class="col-sm-3 control-label">كلمة المرور</label>
                                <div class="col-sm-9">
                                    <input type="password" name="password" class="form-control" id="password"
                                           placeholder="كلمة المرور" required>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="role" class="col-sm-3 control-label">الوظيفة</label>
                                <div class="col-sm-9">
                                    <select name="role" class="form-control select2" id="role" required>
                                        <option value="1">مدير</option>
                                        <option value="0">موظف</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="status" class="col-sm-3 control-label">الحالة</label>
                                <div class="col-sm-9">
                                    <label>
                                        <input type="radio" name="status" value="1" id="status" class="minimal" checked> <span> مفعل</span>
                                    </label>
                                    <label>
                                        <input type="radio" name="status" value="0" class="minimal"> <span> غير مفعل</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group">

                                <div class="col-xs-3">
                                    <button type="submit" class="btn btn-info btn-block pull-right">إضافة</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection