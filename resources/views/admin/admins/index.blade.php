@extends('admin.app')

@section('title', 'المستخدمون')
@section('menu', 2)

@section('content')

    <section class="content-header">
        <h1>
            المستخدمون
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
            <li><i class="fa fa-user-secret"></i> المستخدمون</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">قائمة المستخدمين</h3>
                <div class="pull-left">
                    <a class="btn bg-purple color-palette" href="{{route('admin.admins.create')}}"><i class="fa fa-plus"></i> إضافة</a>
                </div>
                <div class="pull-left" style="margin-left: 10px">
                    <a class="btn bg-yellow color-palette" href="{{route('admin.admins.roles')}}"><i class="fa fa-gears"></i> تعديل صلاحيات الموظفين</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="box-body">
                <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th>الإسم</th>
                        <th>البريد الإلكتروني</th>
                        <th>الوظيفة</th>
                        <th>الحالة</th>
                        <th>الأوامر</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($admins as $admin)
                        <tr>
                            <td class="text-center">{{$loop->index+1}}</td>
                            <td>{{$admin->name}}</td>
                            <td>{{$admin->email}}</td>
                            <td>@if($admin->role==1) مدير @else موظف @endif</td>
                            <td class="text-center">
                                @if($admin->status==1)
                                    <a class="btn btn-success btn-xs">مفعل</a>
                                @else
                                    <a class="btn btn-warning btn-xs">غير مفعل</a>
                                @endif
                            </td>
                            <td class="text-center">
                                <a class="btn btn-primary btn-xs" href="{{route('admin.admins.edit', $admin->id)}}">
                                    <i class="fa fa-edit"></i> <span>تعديل</span>
                                </a>
                                @if(Auth::guard('admin')->user()->id!=$admin->id)
                                    <a class="btn btn-danger btn-xs" href="{{route('admin.admins.delete', $admin->id)}}"
                                       onclick="return confirm('هل انت متأكد؟')">
                                        <i class="fa fa-trash"></i> <span>تعديل</span>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tfoot>
                </table>
            </div>
        </div>
    </section>

@endsection