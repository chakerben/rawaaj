<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>إستعادة كلمة المرور</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link rel="stylesheet" href="{{assetAdmin("bootstrap/css/bootstrap.min.css")}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{assetAdmin("dist/css/AdminLTE.min.css")}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-page">
<div class="login-box">
    <div class="login-logo"><b>رواج</b></div><!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">إستعادة كلمة المرور</p>

        @include("admin.messages")

        <form action="{{route("admin.auth.password.request")}}" method="post">
            {{csrf_field()}}
            <input type="hidden" name="token" value="{{ @$token }}">

            <p style="text-align: center;">
                <small>أدخل كلمة المرور الجديدة</small>
            </p>

            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" name="email" class="form-control" value="{{ $email or old('email') }}"
                       placeholder="البريد الإلكتروني" style="direction: rtl" required autofocus>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" name="password" class="form-control"
                       placeholder="كلمة المرور الجديدة" style="direction: rtl" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input type="password" name="password_confirmation" class="form-control"
                       placeholder="تأكيد كلمة المرور" style="direction: rtl" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>

            <div class="row">
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">حفظ</button>
                </div><!-- /.col -->
                <div class="col-xs-4"></div>
                <div class="col-xs-4">
                    <a class="btn btn-default btn-block btn-flat" href="{{route('admin.auth.login')}}">عودة</a>
                </div>
            </div>
        </form>

        <style>
            .help-block{
                color:#a94442
            }
        </style>

    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<!-- jQuery 2.1.4 -->
<script src="{{assetAdmin("plugins/jQuery/jQuery-2.1.4.min.js")}}"></script>
<!-- Bootstrap 3.3.4 -->
<script src="{{assetAdmin("bootstrap/js/bootstrap.min.js")}}"></script>
<script>
    $(function () {
        setTimeout(function () {
            $(".alert").fadeOut("slow");
        }, 5000);
    });
</script>
</body>
</html>
