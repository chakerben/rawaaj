<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>إستعادة كلمة المرور</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link rel="stylesheet" href="{{assetAdmin("bootstrap/css/bootstrap.min.css")}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{assetAdmin("dist/css/AdminLTE.min.css")}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-page">
<div class="login-box">
    <div class="login-logo"><b>رواج</b></div><!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">إستعادة كلمة المرور</p>

        @include("admin.messages")

        <form action="{{route("admin.auth.password.email")}}" method="post">
            {{csrf_field()}}
            <p style="text-align: center;"><small>أدخل بريدك الإلكتروني كي يصلك رابط صفحة إستعادة كلمة المرور في رسالة</small></p>
            <div class="form-group has-feedback">
                <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="البريد الإلكتروني" style="direction: rtl" required>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">إرسال</button>
                </div><!-- /.col -->
                <div class="col-xs-4"></div>
                <div class="col-xs-4">
                    <a class="btn btn-default btn-block btn-flat" href="{{route('admin.auth.login')}}">عودة</a>
                </div>
            </div>
        </form>


    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<!-- jQuery 2.1.4 -->
<script src="{{assetAdmin("plugins/jQuery/jQuery-2.1.4.min.js")}}"></script>
<!-- Bootstrap 3.3.4 -->
<script src="{{assetAdmin("bootstrap/js/bootstrap.min.js")}}"></script>
<script>
    $(function () {
        setTimeout(function () {
            $(".alert").fadeOut("slow");
        },5000);
    });
</script>
</body>
</html>
