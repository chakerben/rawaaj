@extends('admin.app')

@section('title', 'الإشتراكات')
@section('menu', 5)

@section('content')

    <section class="content-header">
        <h1>
            الإشتراكات
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
            <li><i class="fa fa-thumbs-up"></i> الإشتراكات</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">قائمة الإشتراكات</h3>
            </div>
            <div class="box-body">

                <form class="form-inline filter" action="{{route('dashboard.inscriptions_filter')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="user_id">المسوق : </label>
                        <select name="user_id" class="form-control">
                            <option value="">الكل</option>
                            @foreach($users as $user)
                                <option value="{{$user->id}}"
                                        @if(@$user_id==$user->id) selected @endif>{{$user->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="product_id">المنتج : </label>
                        <select name="product_id" class="form-control">
                            <option value="">الكل</option>
                            @foreach($products as $product)
                                <option value="{{$product->id}}"
                                        @if(@$product_id==$product->id) selected @endif>{{$product->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="created_at">التاريخ : </label>
                        <link rel="stylesheet"
                              href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css"/>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.ar.min.js"></script>
                        <input type="text" name="created_at" class="form-control datepicker" value="{{@$created_at}}">
                    </div>
                    <button type="submit" class="btn btn-primary">فلتر</button>
                    @if(@$user_id or @$product_id or @$created_at)
                        <a class="btn btn-success" href="{{route('dashboard.inscriptions')}}">عودة</a>
                    @endif
                </form>
                <div class="clearfix"></div>

                <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th class="text-center" width="3%"></th>
                        <th class="text-center">المنتج</th>
                        <th class="text-center">المشترك</th>
                        <th class="text-center">الحالة</th>
                        <th class="text-center">تاريخ الإشتراك</th>
                        <th class="text-center">المسوق</th>
                        <th class="text-center">الأوامر</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($inscriptions as $inscription)
                        <tr>
                            <td class="text-center">{{$loop->index+1}}</td>
                            <td>{{$inscription->product->name}}</td>
                            <td>{{$inscription->name}}<br>{{$inscription->email}}</td>
                            <td class="text-center" style="border-left: 1px solid #dddddd;">
                                @if($inscription->status==1)
                                    <a class="btn btn-success btn-xs">مقبول</a>
                                @else
                                    <a class="btn btn-warning btn-xs">في الإنتظار</a>
                                @endif
                            </td>
                            <td class="text-center">{{date('Y/m/d', strtotime($inscription->created_at))}}</td>
                            <td>
                                <a href="{{route('admin.users.edit',$inscription->user->id)}}">{{$inscription->user->name}}</a>
                            </td>
                            <td class="text-center">
                                @if($inscription->status==0)
                                    <a class="btn btn-info btn-xs" href="{{route('dashboard.inscription_status',["id"=>$inscription->id, "status"=>1])}}"><i class="fa fa-thumbs-up"></i> قبول</a>
                                @endif
                                    <a class="btn btn-danger btn-xs" onclick="return confirm('هل أنت متأكد؟')" href="{{route('dashboard.inscription_status',["id"=>$inscription->id, "status"=>2])}}"><i class="fa fa-thumbs-down"></i> حذف</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </section>

    <style>
        .datepicker {
            text-align: right;
            border-radius: 0px;
        }

        select.form-control {
            padding: 2px 12px;
        }

        .filter {
            margin-bottom: 30px;
        }

        .filter button, .filter .btn {
            width: 100px;
        }

        .filter .form-control {
            margin-left: 20px;
        }
    </style>

    <script>
        $(function () {
            $('.datepicker').datepicker({
                language: 'ar',
                format: 'yyyy/mm/dd'
            });
        });
    </script>

@endsection