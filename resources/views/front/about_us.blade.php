@extends("front.app")

@section("title", "نبذة عن رواج")
@section("menu", 2)

@section("content")

    <div class="container" style="margin-top: 100px; background: #f4f4f4; border: 1px solid #dfdfdf; padding: 30px 40px;border-radius: 3px; margin-bottom: 100px">
        {!! $content !!}
    </div>

@endsection