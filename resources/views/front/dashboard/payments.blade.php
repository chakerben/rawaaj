<br>
<p><strong>قائمة التصفيات</strong></p>
<br>
<?php $totall = 0;?>
<table class="table table-bordered table-striped dataTable">
    <thead>
    <tr>
        <th></th>
        <th>اسم المنتج</th>
        <th>المشترك</th>
        <th>تاريخ الإشتراك</th>
        <th>تاريخ التصفية</th>
        <th>نسبة المسوق</th>
        <th style="border-left: 1px solid #dddddd;">المبلغ</th>
        <th class="text-center">الأوامر</th>
    </tr>
    </thead>
    <tbody>
    @foreach($payments as $payment)
        <tr>
            <td class="text-center">{{$loop->index+1}}</td>
            <td>{{$payment->product->name}}</td>
            <td>{{$payment->inscription->name}}</td>
            <td class="text-center">{{date("Y/m/d", strtotime($payment->inscription->created_at))}}</td>
            <td class="text-center">{{date("Y/m/d", strtotime($payment->created_at))}}</td>
            <td class="text-center">{{$payment->percentage}} %</td>
            <td style="border-left: 1px solid #dddddd;" class="text-center">{{$payment->final_price}} رس</td>
            <td class="text-center">
                @if($payment->status==1)
                    <a class="btn btn-success btn-xs">مسدد</a>
                @else
                    <a class="btn btn-warning btn-xs">في الإنتظار</a>
                @endif
            </td>
        </tr>
        @if($payment->status==0)
            <?php $totall+=$payment->final_price; ?>
        @endif
    @endforeach
    </tbody>
</table>
<br>
<strong><span>تاريخ آخر تصفية :</span>
    <span>@if($last_payement) {{date("Y/m/d", strtotime($last_payement->created_at))}} @else لا
        يوجد @endif</span></strong><br>
<strong><span>المبلغ المستحق :</span> <span>{{$totall}}</span> رس</strong>
<br>