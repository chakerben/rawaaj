<br>
<p><strong>قائمة الزوار عن طريق روابطي الخاصة</strong></p>
<br>
<table class="table table-bordered dataTable">
    <thead>
    <tr>
        <th width="3%"></th>
        <th>المنتج</th>
        <th>الدولة</th>
        <th>المدينة</th>
        <th>الآي بي</th>
        <th style="border-left: 1px solid #dddddd;">التاريخ</th>
        <th>الوقت</th>
    </tr>
    </thead>
    <tbody>
    @foreach($visitors as $visitor)
        <tr>
            <td class="text-center">{{$loop->index+1}}</td>
            <td>
                @foreach($products as $product)
                    @if($product->id==$visitor->product_id)
                        <span>{{$product->name}}</span>
                    @endif
                @endforeach
            </td>
            <td>
                <img src="http://www.geonames.org/flags/x/{{strtolower($visitor->country_code)}}.gif" style="width: 20px;border: 1px solid #efefef;">
                {{$visitor->country}}
            </td>
            <td>{{$visitor->city}}</td>
            <td class="text-center">{{$visitor->ip}}</td>
            <td class="text-center" style="border-left: 1px solid #dddddd;">{{date('Y/m/d', strtotime($visitor->created_at))}}</td>
            <td class="text-center">{{date('G:i:s', strtotime($visitor->created_at))}}</td>
        </tr>
    @endforeach
    </tbody>
</table>