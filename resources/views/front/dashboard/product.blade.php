@extends("front.app")

@section("title", $product->name)

@if(Auth::guard("web")->guest())
    @section("menu", 1)
@else
    @section("menu", 5)
@endif

@section("content")

    <div class="container" style="margin-top: 50px; margin-bottom: 100px">
        <h3 class="text-center  "><b>{{$product->name}}</b></h3><br>
        <img class="img" src="{{asset("public/uploads/images/".$product->image)}}">
        <div class="infos">
            @if(Auth::user()->percentage and Auth::user()->percentage!="")
                <p><b>نسبة التسويق : </b><span>{{Auth::user()->percentage}} %</span></p>
            @else
                <p><b>نسبة التسويق : </b><span>{{$product->percentage}} %</span></p>
            @endif
            <p><b>ثمن : </b><span>{{$product->price}} ريال</span></p>
            <p><b>نبذة عن البرنامج : </b><br>{{$product->short_description}}</p>
            <p><b>وصف البرنامج : </b></p>
            <div>{!! $product->description !!}</div>
            @if($product->status==1 and !Auth::guard("web")->guest())
                <a class=" btn btn-market btn-info" href="{{route('dashboard.product_user', $product->id)}}"><i class="fa fa-shopping-cart"></i> سوق هذا المنتج</a>
            @endif
        </div>
    </div>

    <style>
        .img {
            display: block;
            margin: 0px auto 20px;
            border: 1px solid #5e5c66;
        }

        .infos {
            margin: 40px auto;
        }

        @media all and (min-width: 768px) {
            .img, .infos {
                width: 60%;
            }
        }

        @media all and (max-width: 767px) {
            .img, .infos {
                max-width: 100%;
            }
        }

        .btn {
            border-radius: 0px;
            margin-top: 20px;
        }
    </style>

@endsection