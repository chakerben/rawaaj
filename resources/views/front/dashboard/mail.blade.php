<div class="row">
    <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mail-form">
        <h4 class="text-center">مراسلة الإدارة</h4>
        <form action="{{route('dashboard.send')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}

            <div class="form-group">
                <label for="subject">الموضوع</label>
                <input type="text" name="subject" class="form-control" value="{{old('subject')}}" id="subject" required>
            </div>

            <div class="form-group">
                <label for="message">الرسالة</label>
                <textarea name="message" class="form-control" id="message" rows="5" required>{{old('message')}}</textarea>
            </div>

            <div class="form-group">
                <label for="file">ملف مرفق</label>
                <input type="file" name="file" class="form-control" id="file">
            </div>

            {!! NoCaptcha::renderJs('ar') !!}
            <center>{!! NoCaptcha::display(['data-theme' => 'dark']) !!}</center>
            <br>

            <button type="submit" class="btn btn-primary btn-block">أرسل</button>
        </form>
    </div>
</div>

<br><br>

<table class="table table-bordered dataTable">
    <thead>
    <tr>
        <th width="5%"></th>
        <th>الموضوع</th>
        <th>الرسالة</th>
        <th>الحالة</th>
        <th>ملف مرفق</th>
        <th style="border-left: 1px solid #dddddd;">الردود</th>
        <th>التاريخ</th>
    </tr>
    </thead>
    <tbody>
    @foreach($messages as $message)
        <tr>
            <td class="text-center">{{$loop->index+1}}</td>
            <td>{{$message->subject}}</td>
            <td>{{substr($message->message,0,200)}} ...</td>
            <td class="text-center">
                @if($message->status==1)
                    <a class="btn btn-success btn-xs">مقروء</a>
                @else
                    <a class="btn btn-warning btn-xs">غير مقروء</a>
                @endif
            </td>
            <td class="text-center" >
                @if($message->file)
                    <a class="brn btn-danger btn-xs" target="_blank" href="{{asset("public/uploads/messages/".$message->file)}}"><i class="fa fa-download"></i> تنزيل</a>
                @else
                    <span>لا يوجد</span>
                @endif
            </td>
            <td class="text-center" style="border-left: 1px solid #dddddd;">
                @if(count($message->messages)>0)
                    <a class="btn btn-xs btn-info" href="{{route('dashboard.show_responses',$message->id)}}"><i class="fa fa-eye"></i> شاهد الرد</a>
                @else
                    <span>لا يوجد</span>
                @endif
            </td>
            <td class="text-center">{{date("Y/m/d", strtotime($message->created_at))}}</td>
        </tr>
    @endforeach
    </tbody>
</table>

<style>
    .mail-form {
        border: 1px solid #efefef;
        border-radius: 4px;
        padding: 20px;
    }

    .mail-form h4 {
        margin-top: 0px;
        font-weight: bold;
    }

    .mail-form button {
        width: 120px;
        margin: 0px auto;
    }

    .bg-gray {
        color: #000;
        background-color: #d2d6de !important;
    }

    .remodal h4, .remodal h5 {
        font-weight: bold;
    }
</style>