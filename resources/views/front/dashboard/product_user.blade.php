@extends("front.app")

@section("title", $product->name)

@if(Auth::guard("web")->guest())
    @section("menu", 1)
@else
    @section("menu", 5)
@endif

@section("content")

    <script>var surl = "{{@$short_url}}";</script>

    <div class="container" style="margin-top: 50px; margin-bottom: 100px">
        <div class="row">
            <div class="col-sm-3">
                <img class="img" src="{{asset("public/uploads/images/".$product->image)}}">
            </div>
            <div class="col-sm-9">
                <p><h4><b>{{$product->name}}</b></h4></p>
                @if(Auth::user()->percentage and Auth::user()->percentage!="")
                    <p><b>نسبة التسويق : </b><span>{{Auth::user()->percentage}} %</span></p>
                @else
                    <p><b>نسبة التسويق : </b><span>{{$product->percentage}} %</span></p>
                @endif
                <p><b>ثمن : </b><span>{{$product->price}} ريال</span></p>
                <p><b>نبذة عن البرنامج : </b><br>{{$product->short_description}}</p>
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-sm-12">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#nav1"><span>التفاصيل</span></a></li>
                    <li><a data-toggle="tab" href="#nav2">الروابط</a></li>
                    <li><a data-toggle="tab" href="#nav3"><span>الزوار</span> <span>({{count(@$visitors)}})</span></a>
                    </li>
                    <li><a data-toggle="tab" href="#nav4"><span>المشتركون</span> <span>({{count(@$inscriptions)}})</span></a></li>
                </ul>
                <div class="tab-content">
                    <div id="nav1" class="tab-pane fade in active">
                        <div class="col-sm-8 col-sm-offset-2">
                            <div class="data" style="padding-top: 20px">
                                <p><b>نبذة عن البرنامج : </b><br>{{$product->short_description}}</p>
                                <p><b>وصف البرنامج : </b></p>
                                <div>{!! $product->description !!}</div>
                            </div>
                        </div>
                    </div>
                    <div id="nav2" class="tab-pane fade">
                        <div class="col-sm-8 col-sm-offset-2">
                            <div class="data">
                                <h5 class="text-center">الرابط الأصلي</h5>
                                <div class="url">{{$product->url}}</div>
                                <h5 class="text-center">رابط التسويق</h5>
                                <div class="url">{{@$url}}</div>
                                <h5 class="text-center">رابط التسويق القصير</h5>
                                <div class="short_url">{{@$short_url}}</div>
                                <h5 class="text-center">شارك الرابط على</h5>
                                <div class="row">
                                    <div class="col-xs-3"><a class="linkedin btn" href="#"
                                                             onclick="event.preventDefault();linkedin()"><i
                                                    class="fa fa-linkedin"></i> <span>لينكدإن</span></a>
                                    </div>
                                    <div class="col-xs-3"><a class="google btn" href="#"
                                                             onclick="event.preventDefault();google()"><i
                                                    class="fa fa-google-plus"></i> <span>غوغل</span></a>
                                    </div>
                                    <div class="col-xs-3"><a class="facebook btn" href="#"
                                                             onclick="event.preventDefault();facebook()"><i
                                                    class="fa fa-facebook"></i> <span>الفايسبوك</span></a>
                                    </div>
                                    <div class="col-xs-3"><a class="twitter btn" href="#"
                                                             onclick="event.preventDefault();twitter()"><i
                                                    class="fa fa-twitter"></i> <span>التويتر</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="nav3" class="tab-pane fade">
                        <br>
                        <p><strong>قائمة الزوار عن طريق روابطي الخاصة لهذا المنتج</strong></p>
                        <br>
                        <table class="table table-bordered dataTable">
                            <thead>
                            <tr>
                                <th width="3%"></th>
                                <th>الدولة</th>
                                <th>المدينة</th>
                                <th>الآي بي</th>
                                <th style="border-left: 1px solid #dddddd;">التاريخ</th>
                                <th>الوقت</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($visitors as $visitor)
                                <tr>
                                    <td class="text-center">{{$loop->index+1}}</td>
                                    <td class="text-center">
                                        <img src="http://www.geonames.org/flags/x/{{strtolower($visitor->country_code)}}.gif" style="width: 20px;border: 1px solid #efefef;">
                                        {{$visitor->country}}
                                    </td>
                                    <td class="text-center">{{$visitor->city}}</td>
                                    <td class="text-center">{{$visitor->ip}}</td>
                                    <td class="text-center" style="border-left: 1px solid #dddddd;">{{date('Y/m/d', strtotime($visitor->created_at))}}</td>
                                    <td class="text-center">{{date('G:i:s', strtotime($visitor->created_at))}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div id="nav4" class="tab-pane fade">
                        <br>
                        <p><strong>قائمة المشتركين عن طريق روابطي الخاصة لهذا المنتج</strong></p>
                        <br>
                        <table class="table table-bordered dataTable">
                            <thead>
                            <tr>
                                <th width="3%"></th>
                                <th>الإسم</th>
                                <th>الدولة</th>
                                <th>المدينة</th>
                                <th>الآي بي</th>
                                <th style="border-left: 1px solid #dddddd;">الحالة</th>
                                <th>التاريخ</th>
                                <th>الوقت</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($inscriptions as $inscription)
                                <tr>
                                    <td class="text-center">{{$loop->index+1}}</td>
                                    <td>{{$inscription->name}}</td>
                                    <td>
                                        <img src="http://www.geonames.org/flags/x/{{strtolower($inscription->country_code)}}.gif" style="width: 20px;border: 1px solid #efefef;">
                                        {{$inscription->country}}
                                    </td>
                                    <td>{{$inscription->city}}</td>
                                    <td class="text-center">{{$inscription->ip}}</td>
                                    <td class="text-center" style="border-left: 1px solid #dddddd;">
                                        @if($inscription->status==1)
                                            <a class="btn btn-success btn-xs">مقبول</a>
                                        @else
                                            <a class="btn btn-warning btn-xs">في الإنتظار</a>
                                        @endif
                                    </td>
                                    <td class="text-center">{{date('Y/m/d', strtotime($inscription->created_at))}}</td>
                                    <td class="text-center">{{date('G:i:s', strtotime($inscription->created_at))}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container" style="margin-top: 50px; margin-bottom: 100px">
        <h3 class="text-center  "><b>{{$product->name}}</b></h3><br>
        <img class="img" src="{{asset("public/uploads/images/".$product->image)}}">
        <div class="infos">
            @if(Auth::user()->percentage and Auth::user()->percentage!="")
                <p><b>نسبة التسويق : </b><span>{{Auth::user()->percentage}} %</span></p>
            @else
                <p><b>نسبة التسويق : </b><span>{{$product->percentage}} %</span></p>
            @endif
            <p><b>ثمن : </b><span>{{$product->price}} ريال</span></p>
            <p><b>نبذة عن البرنامج : </b><br>{{$product->short_description}}</p>


        </div>
    </div>

    <style>
        .img {
            display: block;
            border: 1px solid #5e5c66;
            width: 100%;
        }

        .infos {
            margin: 40px auto;
        }

        @media all and (min-width: 768px) {

            .infos {
                width: 60%;
            }
        }

        @media all and (max-width: 767px) {
            .img, .infos {
                max-width: 100%;
            }
        }

        .btn {
            border-radius: 0px;
        }

        .url, .short_url {

        }

        .google, .facebook, .twitter, .linkedin {
            display: block;
            text-align: center;
            color: #fff;
        }

        .linkedin:hover, .linkedin:active, .google:hover, .facebook:hover, .twitter:hover, .google:active, .facebook:active, .twitter:active {
            color: #fff;
        }

        .linkedin .fa, .google .fa, .facebook .fa, .twitter .fa {
            display: block;
            text-align: center;
            font-size: 22px;
            margin-bottom: 10px;
            color: #fff;
        }

        .linkedin {
            background: darkblue;
        }

        .google {
            background: #e8332f;
        }

        .facebook {
            background: #0d71bb;
        }

        .twitter {
            background: #0d5a9c;
        }

        .data {
            border: 1px solid #efefef;
            padding: 0px 20px 20px;
            margin-top: 30px;
        }

        .data .url, .data .short_url {
            padding: 2px 0px;
            color: #027a98;
            background: #eee;
            border: 1px solid #c4c4c4;
            width: 80%;
            margin: 0px auto;
            text-align: center;
        }

        .data h5 {
            font-weight: bold;
            margin-top: 20px;
            margin-bottom: 20px;
        }
    </style>

    <script>
        function google() {
            var link = "https://plus.google.com/share?url=" + surl;
            window.open(link, 'مشاركة على  غوغل +', 'width=400, height=300');
        }

        function facebook() {
            var link = "https://www.facebook.com/sharer/sharer.php?u=" + surl;
            window.open(link, 'مشاركة على  فايسبوك', 'width=400, height=300');
        }

        function twitter() {
            var link = "https://twitter.com/home?status=" + surl;
            window.open(link, 'مشاركة على  تويتر', 'width=400, height=300');
        }

        function linkedin() {
            var link = "https://www.linkedin.com/shareArticle?mini=true&url=" + surl + "&title=&summary=&source=";
            window.open(link, 'مشاركة على  تويتر', 'width=400, height=300');
        }
    </script>

    @
    <style>
        th {
            background: #efefef;
            text-align: center;
        }

        .span-block {
            width: 115px;
            display: inline-block;
        }

        .show-code {
            color: #027a98;
            display: inline-block;
            width: 130px;
        }

        .nav.nav-tabs li:not(.active) a {
            color: #333;
        }

        .nav.nav-tabs li:not(.active) a:hover {
            color: #fff;
        }

        .tab-content {
            min-height: 500px;
            border: 1px solid #ddd;
            border-top: none;
            margin-bottom: 100px;
            margin-top: 0px;
            padding: 20px;
        }

        select.form-control {
            padding: 2px 12px;
        }
    </style>

@endsection

@section("scripts")
    <!-- DataTables -->
    <script src="{{assetAdmin('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{assetAdmin('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('.dataTable').DataTable({
                language: {
                    "sProcessing": "جارٍ التحميل...",
                    "sLengthMenu": "أظهر  _MENU_  نتائج",
                    "sZeroRecords": "لم يعثر على أية نتائج",
                    "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ نتيجة",
                    "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 نتيجة",
                    "sInfoFiltered": "(منتقاة من مجموع _MAX_ نتائج)",
                    "sInfoPostFix": "",
                    "sSearch": "بحث : ",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "الأول",
                        "sPrevious": "السابق",
                        "sNext": "التالي",
                        "sLast": "الأخير"
                    }
                }
                ,
                "pageLength": 25
            });
        });
    </script>
@endsection