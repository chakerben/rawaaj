@extends("front.app")

@section("title", "حسابي الشخصي")
@section("menu", 5)

@section("content")

    <div class="container" style="margin-top: 50px; margin-bottom: 100px">

        <form action="{{route('dashboard.resend', $message->id)}}" method="post"
              class="form-horizontal" enctype="multipart/form-data">
            {{csrf_field()}}

            <h5><span>الرسالة</span> <i class="bg-gray color-palette"
                                        style="padding: 0px 10px;margin-right: 10px;">{{date("G:i Y/m/d", strtotime($message->created_at))}}</i>
            </h5>
            <p style="text-align: justify">{{$message->message}}</p>
            @if($message->file)
                <a target="_blank" href="{{asset("public/uploads/messages/".$message->file)}}"><i class="fa fa-link"></i> ملف مرفق</a>
            @endif
            <hr>

            <h4 style="margin-bottom: 20px;">الردود : </h4>
            @foreach($message->messages as $m)
                <h5>
                    @if($m->response_from==0)
                        <span>الإدارة</span>
                    @else
                        <span>{{$message->user->name}}</span>
                    @endif
                    <i class="bg-gray color-palette"
                       style="padding: 0px 10px;margin-right: 10px;">{{date("G:i Y/m/d", strtotime($m->created_at))}}</i>
                </h5>
                <p style="text-align: justify">{{$m->message}}</p>
                @if($m->file)
                    <a target="_blank" href="{{asset("public/uploads/messages/".$m->file)}}"><i class="fa fa-link"></i> ملف مرفق</a>
                @endif
                <hr>
            @endforeach

            <div class="box-body">
                <div class="form-group">
                    <label for="message" class="col-sm-1 control-label">الرد</label>
                    <div class="col-sm-11">
                                    <textarea required name="message" class="form-control" id="message"
                                              rows="6"></textarea>
                    </div>
                </div>
            </div>

            <div class="box-body">
                <div class="form-group">
                    <label for="file" class="col-sm-1 control-label">ملف مرفق</label>
                    <div class="col-sm-11">
                        <input type="file"  name="file" class="form-control" id="file">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-11 col-sm-offset-1">
                    {!! NoCaptcha::renderJs('ar', true, 'recaptchaCallback') !!}
                    {!! NoCaptcha::display(['data-theme' => 'dark']) !!}
                    <script>
                        function recaptchaCallback() {
                            document.querySelectorAll('.g-recaptcha').forEach(function (el) {
                                grecaptcha.render(el);
                            });
                        }
                    </script>
                </div>
            </div>

            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-1">
                        <button type="submit" class="btn btn-info btn-block pull-right">إرسال</button>
                    </div>
                </div>
            </div>

        </form>

        <style>
            form h4, form h5{
                font-weight: bold;
            }
            .bg-gray {
                color: #000;
                background-color: #d2d6de !important;
            }
        </style>

    </div>

@endsection