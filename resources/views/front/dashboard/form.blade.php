<div class="col-sm-8 col-sm-offset-2" style="margin-top: 30px">

    <form action="{{route('dashboard.save_infos')}}" method="post" class="form-horizontal">
        {{csrf_field()}}

        <div class="box-body">
            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">الإسم <span class="colorred">*</span></label>
                <div class="col-sm-9">
                    <input value="{{$user->name}}" type="text" name="name" class="form-control" id="name" placeholder="الإسم" required>
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">البريد الإلكتروني <span class="colorred">*</span></label>
                <div class="col-sm-9">
                    <input value="{{$user->email}}" type="email" name="email" class="form-control" id="email" placeholder="البريد الإلكتروني" required>
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                <label for="phone" class="col-sm-3 control-label">رقم الجوال <span class="colorred">*</span></label>
                <div class="col-sm-9">
                    <input value="{{$user->phone}}" type="tel" name="phone" class="form-control" id="phone" placeholder="رقم الجوال" required>
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                <label for="account" class="col-sm-3 control-label">رقم الحساب البنكي <span class="colorred">*</span></label>
                <div class="col-sm-9">
                    <input value="{{$user->account}}" type="text" name="account" class="form-control" id="account" placeholder="رقم الحساب البنكي" required>
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                <label for="age" class="col-sm-3 control-label">العمر</label>
                <div class="col-sm-9">
                    <input value="{{$user->age}}" type="number" min="18" max="60" name="age" class="form-control" id="age" placeholder="العمر" style="width:80px">
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                <label for="gender" class="col-sm-3 control-label">الجنس</label>
                <div class="col-sm-9">
                    <select name="gender" id="gender" class="form-control" style="width:150px" plac>
                        <option value=""></option>
                        <option value="1" @if($user->gender==1) selected @endif>ذكر</option>
                        <option value="2" @if($user->gender==2) selected @endif>أنثى</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                <label for="city" class="col-sm-3 control-label">المدينة</label>
                <div class="col-sm-9">
                    <input value="{{$user->city}}" type="text" name="city" maxlength="100" class="form-control" id="city" style="width:150px" placeholder="المدينة">
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                <label for="availability" class="col-sm-3 control-label">التفرغ للعمل كمسوق</label>
                <div class="col-sm-9">
                    <textarea name="availability" id="availability" class="form-control" rows="3" maxlength="200" placeholder="هل أنت متفرغ للعمل كمسوق ؟ لأي درجة ؟">{{$user->availability}}</textarea>
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                <label for="interests" class="col-sm-3 control-label">المجالات والاهتمامات</label>
                <div class="col-sm-9">
                    <textarea name="interests" id="interests" class="form-control" rows="6" placeholder="إحكي لنا عن نفسك قليلا ...">{{$user->interests}}</textarea>
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                <label for="interests" class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    {!! NoCaptcha::renderJs('ar', true, 'recaptchaCallback') !!}
                    {!! NoCaptcha::display(['data-theme' => 'dark']) !!}
                    <script>
                        function recaptchaCallback() {
                            document.querySelectorAll('.g-recaptcha').forEach(function (el) {
                                grecaptcha.render(el);
                            });
                        }
                    </script>
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                <div class="col-sm-3 col-sm-offset-3" style="margin-top: 30px">
                    <button type="submit" class="btn btn-primary btn-block">تعديل البيانات</button>
                    <br>
                    <small><span class="colorred">*</span> : <span>حقول إلزامية</span></small>
                </div>
            </div>
        </div>

    </form>

</div>

<div class="clearfix"></div>


