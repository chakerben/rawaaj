@extends("front.app")

@section("title", "حسابي الشخصي")
@section("menu", 5)

@section("styles")
    <!-- DataTables -->
    <link rel="stylesheet" href="{{assetAdmin('plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section("content")

    <div class="container" style="margin-top: 50px">
        <h3 class="text-center">مرحبا بك {{$user->name}}</h3>
        <h5>
            <span class="span-block">كود المسوق :</span>
            <input readonly value="{{$user->code}}" class="form-control show-code">
        </h5>
        <h5>
            <span class="span-block">عمولة التسويق :</span>
            <input readonly value="@if($user->percentage){{$user->percentage}} % @else حسب المنتج @endif"
                   class="form-control show-code">
        </h5>

        <hr style="margin: 40px 0px">

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#nav1"><span>المنتجات</span> <span>({{count($products)}}
                        )</span></a></li>
            <li><a data-toggle="tab" href="#nav2"><span>الزوار</span> <span>({{count($visitors)}})</span></a></li>
            <li><a data-toggle="tab" href="#nav3"><span>المشتركون</span> <span>({{count($inscriptions)}})</span></a>
            </li>
            <li><a data-toggle="tab" href="#nav4">التقرير المالي</a></li>
            <li><a data-toggle="tab" href="#nav5">بياناتي الشخصية</a></li>
            <li><a data-toggle="tab" href="#nav6">الرسائل</a></li>

        </ul>

        <div class="tab-content">
            <div id="nav1" class="tab-pane fade in active">
                @include("front.dashboard.products")
            </div>
            <div id="nav2" class="tab-pane fade">
                @include("front.dashboard.visitors")
            </div>
            <div id="nav3" class="tab-pane fade">
                @include("front.dashboard.inscriptions")
            </div>
            <div id="nav4" class="tab-pane fade">
                @include("front.dashboard.payments")
            </div>
            <div id="nav5" class="tab-pane fade">
                @include("front.dashboard.form")
            </div>
            <div id="nav6" class="tab-pane fade">
                @include("front.dashboard.mail")
            </div>
        </div>
    </div>

    <style>
        th {
            background: #efefef;
            text-align: center;
        }

        .span-block {
            width: 115px;
            display: inline-block;
        }

        .show-code {
            color: #027a98;
            display: inline-block;
            width: 130px;
        }

        .nav.nav-tabs li:not(.active) a {
            color: #333;
        }

        .nav.nav-tabs li:not(.active) a:hover {
            color: #fff;
        }

        .tab-content {
            min-height: 500px;
            border: 1px solid #ddd;
            border-top: none;
            margin-bottom: 100px;
            margin-top: 0px;
            padding: 20px;
        }

        select.form-control {
            padding: 2px 12px;
        }
    </style>

@endsection

@section("scripts")
    <!-- DataTables -->
    <script src="{{assetAdmin('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{assetAdmin('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('.dataTable').DataTable({
                language: {
                    "sProcessing": "جارٍ التحميل...",
                    "sLengthMenu": "أظهر  _MENU_  نتائج",
                    "sZeroRecords": "لم يعثر على أية نتائج",
                    "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ نتيجة",
                    "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 نتيجة",
                    "sInfoFiltered": "(منتقاة من مجموع _MAX_ نتائج)",
                    "sInfoPostFix": "",
                    "sSearch": "بحث : ",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "الأول",
                        "sPrevious": "السابق",
                        "sNext": "التالي",
                        "sLast": "الأخير"
                    }
                }
                ,
                "pageLength": 10
            });
        });
    </script>
@endsection