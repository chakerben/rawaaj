<script>var surl = "";</script>
<div class="row products">

    <div class="panel panel-danger">
        <div class="panel-heading"><h5>منتجات مشترك في تسويقها</h5></div>
        <div class="panel-body">
            <?php $num=0;?>
            @foreach($products as $product)
                @if($product->pivoted)
                    <?php $num++;?>
                    <div class="col-md-3 col-sm-4">
                    <div class="box">
                        <a class="img-wrapper" href="{{route('dashboard.product', $product->id)}}" style="background-image: url({{asset("public/uploads/images/".$product->image)}})">
                            <span class="price">{{$product->price}} ريال</span>
                            @if($user->percentage and $user->percentage!="")
                                <span class="percentage">{{$user->percentage}}%</span>
                            @else
                                <span class="percentage">{{$product->percentage}}%</span>
                            @endif
                        </a>
                        <h5>{{$product->name}}</h5>
                        <p class="desc">{{$product->short_description}}</p>
                        <div class="row">
                            <div class="col-xs-5">
                                <a class="btn btn-info btn-block btn-xs text-center" href="{{route('dashboard.product', $product->id)}}"><i
                                            class="fa fa-eye"></i> <span>التفاصيل</span>
                                </a>
                            </div>
                            <div class="col-xs-7">
                                @if($product->status==1)
                                    <a class="btn btn-default btn-block btn-xs text-center" href="{{route('dashboard.product_user', $product->id)}}">
                                        <i class="fa fa-shopping-cart"></i> <span>سوق هذا المنتج</span>
                                    </a>
                                @else
                                    <a class="btn btn-default btn-block btn-xs text-center" disabled="" href="#">
                                        <i class="fa fa-shopping-cart"></i> <span>سوق هذا المنتج</span>
                                    </a>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
                @endif
            @endforeach
            @if($num==0)
                <h5 class="text-center">لا توجد حاليا أية أنت منتجات مشترك في تسويقها</h5>
            @endif
        </div>
    </div>

    <div class="panel panel-danger">
        <div class="panel-heading"><h5>فرصة تسويقية</h5></div>
        <div class="panel-body">
            @foreach($products as $product)
                @if(!$product->pivoted)
                    <div class="col-md-3 col-sm-4">
                        <div class="box">
                            <a class="img-wrapper" href="{{route('dashboard.product', $product->id)}}" style="background-image: url({{asset("public/uploads/images/".$product->image)}})">
                                <span class="price">{{$product->price}} ريال</span>
                                @if($user->percentage and $user->percentage!="")
                                    <span class="percentage">{{$user->percentage}}%</span>
                                @else
                                    <span class="percentage">{{$product->percentage}}%</span>
                                @endif
                            </a>
                            <h5>{{$product->name}}</h5>
                            <p class="desc">{{$product->short_description}}</p>
                            <div class="row">
                                <div class="col-xs-5">
                                    <a class="btn btn-info btn-block btn-xs text-center" href="{{route('dashboard.product', $product->id)}}"><i
                                                class="fa fa-eye"></i> <span>التفاصيل</span>
                                    </a>
                                </div>
                                <div class="col-xs-7">
                                    @if($product->status==1)
                                        <a class="btn btn-default btn-block btn-xs text-center" href="{{route('dashboard.product_user', $product->id)}}">
                                            <i class="fa fa-shopping-cart"></i> <span>سوق هذا المنتج</span>
                                        </a>
                                    @else
                                        <a class="btn btn-default btn-block btn-xs text-center" disabled="" href="#">
                                            <i class="fa fa-shopping-cart"></i> <span>سوق هذا المنتج</span>
                                        </a>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>

</div>

<style>
    .panel-danger .panel-heading h5{
        color: #ffff;
        margin: 0px;
        font-weight: bold;
    }
    .products {
        margin-top: 10px;
    }

    .box {
        width: 100%;
        padding: 10px;
        border: 1px solid #b0b0b0;
        margin-bottom: 20px;
    }

    .box .img-wrapper {
        position: relative;
        width: 100%;
        display: block;
        min-height: 167px;
        background-size: cover;
        background-position: center center;
    }

    .box .img-wrapper .price {
        color: #fff;
        text-shadow: 1px 1px #333;
        position: absolute;
        bottom: -2px;
        right: 2px;
    }

    .box .img-wrapper .percentage {
        color: #fff;
        text-shadow: 1px 1px #333;
        position: absolute;
        bottom: -2px;
        left: 2px;
    }

    .box img {
        width: 100%;
    }

    .box h5 {
        text-align: center;
        font-weight: bold;
        overflow: hidden;
        line-height: 20px;
    }

    .box h6 {
        font-weight: bold;
    }

    .box .desc {
        text-align: justify;
        height: 124px;
        overflow: hidden;
    }

    .box .btn {
        border-radius: 0px;
    }
</style>

<style>
    .google, .facebook, .twitter, .linkedin {
        display: block;
        text-align: center;
        color: #fff;
    }

    .linkedin:hover, .linkedin:active, .google:hover, .facebook:hover, .twitter:hover, .google:active, .facebook:active, .twitter:active {
        color: #fff;
    }

    .linkedin .fa, .google .fa, .facebook .fa, .twitter .fa {
        display: block;
        text-align: center;
        font-size: 22px;
        margin-bottom: 10px;
        color: #fff;
    }

    .linkedin {
        background: darkblue;
    }

    .google {
        background: #e8332f;
    }

    .facebook {
        background: #0d71bb;
    }

    .twitter {
        background: #0d5a9c;
    }

    .remodal h3 {
        margin-bottom: 20px;
        margin-top: 0px;
    }

    .reloading {
        color: #bc1e2c;
        text-align: center;
        margin-top: 20px;
    }

    .data {
        display: block;
    }

    .data .url, .data .short_url {
        padding: 2px 0px;
        color: #027a98;
        background: #eee;
        border: 1px solid #c4c4c4;
    }
</style>

<script>

    $(document).on('closed', '.remodal', function () {
        surl = "";
        $(".data").fadeOut("fast");
        $(".reloading").fadeIn("fast");
        $(".data .url").html('');
        $(".data .short_url").html('');
    });

    $(document).on('opened', '.remodal', function () {
        var url = $(this).data("url");
        $.ajax({
            method: "GET",
            url: url,
            dataType: "json"
        }).done(function (data) {
            $(".reloading").fadeOut(100);
            setTimeout(function () {
                $(".data .url").html('<a href="' + data.url + '">' + data.url + '</a>');
                $(".data .short_url").html('<a href="' + data.short_url + '">' + data.short_url + '</a>');
                surl = data.short_url;
                $(".data").fadeIn("slow");
            }, 110);
        }).fail(function () {
            $(".data").fadeOut("fast");
            $(".reloading").fadeIn("fast");
            //alert("لقد وقع خطأ ما, الرجاء إعادة تحميل الصفحة و المحاولة من جديد");
        });
    });

    function google() {
        var link = "https://plus.google.com/share?url=" + surl;
        window.open(link, 'مشاركة على  غوغل +', 'width=400, height=300');
    }

    function facebook() {
        var link = "https://www.facebook.com/sharer/sharer.php?u=" + surl;
        window.open(link, 'مشاركة على  فايسبوك', 'width=400, height=300');
    }

    function twitter() {
        var link = "https://twitter.com/home?status=" + surl;
        window.open(link, 'مشاركة على  تويتر', 'width=400, height=300');
    }

    function linkedin() {
        var link = "https://www.linkedin.com/shareArticle?mini=true&url=" + surl + "&title=&summary=&source=";
        window.open(link, 'مشاركة على  تويتر', 'width=400, height=300');
    }

</script>