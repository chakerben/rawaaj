<!DOCTYPE html>
<html>

@include("front.includes.head")

<body>

<div id="overlay" style=""></div>

@include("front.includes.topbar")

<div class="content">
    
    @section("content")@show
    
</div>

@include("front.includes.footer")

</body>
</html>