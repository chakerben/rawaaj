@extends("front.app")

@section("title", "الشروط و الأحكام")
@section("menu", 1)

@section("content")

    <div class="container" style="margin-top: 100px; background: #f4f4f4; border: 1px solid #dfdfdf; padding: 30px 40px;border-radius: 3px; margin-bottom: 100px">
        {!! $conditions !!}
    </div>

@endsection