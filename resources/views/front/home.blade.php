@extends("front.app")

@section("title", "الرئيسية")
@section("menu", 1)

@section("content")

    <div class="container-fluid topcontent">
        <div class="row">
            <div class="col-sm-6 box">
                <div class="text">
                    <h2>التسويق الجماعي رواج</h2>
                    <p>{{$settings[1]->value}}</p>
                    <a href="{{route('about_us')}}">المزيد من التفاصيل</a>
                </div>
            </div>
            <div class="col-sm-6 box">
                <div class="img"></div>
            </div>
        </div>
    </div>

    <div class="container-fluid content2">
        <div class="row bg">
            <div class="col-sm-5">
                <img src="{{assetFront('images/logo.png')}}">
            </div>
            <div class="col-sm-7">
                <h3>التسويق الجماعي رواج</h3>
                <p>{{$settings[2]->value}}</p>
                @if(Auth::guard("web")->guest())
                    <a href="#register">إشترك الآن</a>
                @endif
            </div>
        </div>
    </div>

    @if(Auth::guard("web")->guest())
        <div class="container-fluid content3">
        <div class="row">
            <div class="col-sm-7 box red">
                <h2>الإشتراك في رواج</h2>
                <p>{{$settings[3]->value}}</p>
            </div>
            <div class="col-sm-5 box register-wrapper" id="register">
                <form class="theform" action="{{ route('register') }}" method="post">
                    {{csrf_field()}}
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input id="name" type="text" value="{{old('name')}}" class="form-control" name="name" placeholder="إسمك الكريم" required>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope-open-o"></i></span>
                        <input id="email" type="email" value="{{old('email')}}" class="form-control" name="email" placeholder="البريد الإلكتروني" required>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input id="password" type="password" class="form-control" name="password" placeholder="كلمة المرور" minlength="6" required>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" placeholder="كلمة المرور" minlength="6" required>
                    </div>
                    <div class="input-group" style="margin-bottom: 10px">
                        <span class="input-group-addon"><i class="fa fa-volume-control-phone"></i></span>
                        <input id="phone" type="tel" value="{{old('phone')}}" class="form-control" name="phone" placeholder="رقم الجوال" maxlength="20" required>
                    </div>
                    <a href="#" class="show-summary" onclick="event.preventDefault();$('.summary').slideToggle()">ملخص عن فكرة التسجيل كمسوق</a>
                    <div class="summary">{!! $settings[5]->value !!}</div>
                    <div style="margin-bottom: 12px">
                        <input type="checkbox" class="minimal" name="conditions" value="1"> الموافقة على <a href="{{route('conditions')}}" style="text-decoration: underline">الشروط و الأحكام</a>
                    </div>
                    {!! NoCaptcha::renderJs('ar', true, 'recaptchaCallback') !!}
                    {!! NoCaptcha::display(['data-theme' => 'dark']) !!}
                    <script>
                        function recaptchaCallback() {
                            document.querySelectorAll('.g-recaptcha').forEach(function (el) {
                                grecaptcha.render(el);
                            });
                        }
                    </script>
                    <button style="margin-top: 10px;" type="submit" class="register-submit">الإشتراك في رواج</button>
                </form>
            </div>
        </div>
    </div>
    @endif

@endsection