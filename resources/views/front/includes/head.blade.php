<head>
    <meta charset="UTF-8">
    <title>رواج | @yield("title")</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Remodal -->
    <link rel="stylesheet" href="{{assetFront('css/remodal-default-theme.css')}}">
    <link rel="stylesheet" href="{{assetFront('css/remodal.css')}}">
    <!-- Bootstrap 3.3.4 -->
    <link rel="stylesheet" href="{{assetFront('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{assetFront('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{assetFront('css/bootstrap.rtl.min.css')}}">
    <link rel="stylesheet" href="{{assetFront('css/style.css')}}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{assetAdmin('plugins/iCheck/flat/blue.css')}}">
    <link rel="stylesheet" href="{{assetAdmin('plugins/iCheck/all.css')}}">

    @section('styles')@show

    <!-- jQuery 2.1.4 -->
    <script src="{{assetFront('js/jQuery-2.1.4.min.js')}}"></script>
</head>