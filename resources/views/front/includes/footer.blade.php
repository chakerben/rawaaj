<footer class="container-fluid">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">© جميع الحقوق محفوظة سمارت لايف 2018</div>
    <div class="col-sm-1"></div>
    <div class="col-sm-2">
        <a href="http://smartlives.ws/"><img style="height: 25px;" src="{{assetFront('images/smartlife.png')}}"></a>
    </div>
</footer>

<script src="{{assetFront('js/remodal.min.js')}}"></script>
<script src="{{assetFront('js/bootstrap.min.js')}}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{assetAdmin('plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{assetFront('js/script.js')}}"></script>

@if ($errors->any())
    <div class="error-modal" data-remodal-id="modal">
        <button data-remodal-action="close" class="remodal-close"></button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <script>
        jQuery(function ($) {
            var inst = $('.error-modal').remodal();
            inst.open();
            setTimeout(function () {
                inst.close();
            }, 10000);
        });
    </script>
@endif

@if (session('status'))
    <div class="success-modal" data-remodal-id="modal">
        <button data-remodal-action="close" class="remodal-close"></button>
        <p style="color: #41e757;"><b>{{ session('status') }}</b></p>
    </div>
    <script>
        jQuery(function ($) {
            var inst = $('.success-modal').remodal();
            inst.open();
            setTimeout(function () {
                inst.close();
            }, 10000);
        });
    </script>
@endif

@section('scripts')@show