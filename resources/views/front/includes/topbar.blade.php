<div class="container-fluid topbar">
    <div class="col-sm-3">
        <a href="{{route('home')}}" class="logo"><img src="{{assetFront('images/logo.png')}}"></a>
    </div>
    <div class="col-sm-6 navbar-wrapper">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand hidden-lg hidden-md hidden-sm" href="#">القائمة الرئيسية</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                @if(Auth::guard("web")->guest())
                <li class="@if($__env->yieldContent('menu')==1) active @endif"><a href="{{route('home')}}">الرئيسية</a></li>
                <li class="@if($__env->yieldContent('menu')==2) active @endif"><a href="{{route('about_us')}}">نبذة عن رواج</a></li>
                <li class="@if($__env->yieldContent('menu')==3) active @endif"><a href="{{route('home')}}/#register">إشترك في رواج</a></li>
                <li class="@if($__env->yieldContent('menu')==4) active @endif"><a href="{{route('contact_us')}}">إتصل بنا</a></li>
                @else
                    <li class="@if($__env->yieldContent('menu')==1) active @endif"><a href="{{route('home')}}"><i class="fa fa-home"></i></a></li>
                    <li class="@if($__env->yieldContent('menu')==5) active @endif"><a href="{{route('dashboard')}}">حسابي</a></li>
                    <li class="@if($__env->yieldContent('menu')==2) active @endif"><a href="{{route('about_us')}}">نبذة عن رواج</a></li>
                    <li class="@if($__env->yieldContent('menu')==4) active @endif"><a href="{{route('contact_us')}}">إتصل بنا</a></li>
                @endif
            </ul>
        </div>
    </div>
    <div class="col-sm-3 login-link-wrapper">
        <a class="login-link" href="#">
            <img src="{{assetFront('images/user.png')}}">
        </a>
        @if(Auth::guard("web")->guest())
            <div class="login-wrapper">
                <form class="theform" action="{{ route('login') }}" method="post">
                    {{csrf_field()}}
                    <h5>تسجيل الدخول في رواج</h5>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope-open-o"></i></span>
                        <input id="email" type="email" class="form-control" name="email" placeholder="البريد الإلكتروني" required>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input id="password" type="password" class="form-control" name="password" placeholder="كلمة المرور" required>
                    </div>
                    {!! NoCaptcha::renderJs('ar', true, 'recaptchaCallback') !!}
                    {!! NoCaptcha::display(['data-theme' => 'dark']) !!}
                    <script>
                        function recaptchaCallback() {
                            document.querySelectorAll('.g-recaptcha').forEach(function (el) {
                                grecaptcha.render(el);
                            });
                        }
                    </script>
                    <button type="submit" class="login-submit">تسجيل الدخول</button>
                    <a class="alink" href="{{ route('password.request') }}">نسيت كلمة المرور ؟</a>
                </form>
            </div>
        @else
            <div class="login-wrapper in">
                <form class="theform" id="logoutForm" action="{{ route('logout') }}" method="post">
                    {{csrf_field()}}
                    <a class="alink" href="{{route('dashboard')}}" style="margin-bottom: 10px;">حسابي الشخصي</a>
                    <a class="alink" href="#" onclick="event.preventDefault();getElementById('logoutForm').submit()">تسجيل الخروج</a>
                </form>
            </div>
        @endif
    </div>
</div>