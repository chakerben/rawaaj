@extends("front.app")

@section("title", "نبذة عن رواج")
@section("menu", 4)

@section("content")

    <div class="container"
         style="margin-top: 100px; border: 1px solid #dfdfdf; padding: 30px 40px;border-radius: 3px; margin-bottom: 100px">
        <div class="col-sm-5">
            <h4 class="text-center">بيانات الموقع</h4>
            <ul class="infos">
                <li><i class="fa fa-envelope"></i> <span>البريد الإلكتروني</span> <br> {{$email}}</li>
                <li><i class="fa fa-phone"></i> <span>الهاتف</span> <br> {{$phone}}</li>
                <li><i class="fa fa-fax"></i> <span>الفاكس</span> <br> {{$fax}}</li>
                <li><i class="fa fa-map-marker"></i> <span>العنوان</span> <br> {{$address}}</li>
            </ul>
        </div>
        <div class="col-sm-7">
            <h4 class="text-center">مراسلة الإدارة</h4>
            <form action="{{route('save_contact_us')}}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="name">إسم الكريم</label>
                    <input type="text" name="name" class="form-control" value="{{old('name')}}" id="name" required>
                </div>
                <div class="form-group">
                    <label for="email">البريد الإلكتروني</label>
                    <input type="email" name="email" class="form-control" value="{{old('email')}}" id="email" required>
                </div>
                <div class="form-group">
                    <label for="subject">الموضوع</label>
                    <input type="text" name="subject" class="form-control" value="{{old('subject')}}" id="subject" required>
                </div>
                <div class="form-group">
                    <label for="message">الرسالة</label>
                    <textarea name="message" class="form-control" id="message" rows="5" required>{{old('message')}}</textarea>
                </div>

                {!! NoCaptcha::renderJs('ar', true, 'recaptchaCallback') !!}
                {!! NoCaptcha::display(['data-theme' => 'dark']) !!}
                <script>
                    function recaptchaCallback() {
                        document.querySelectorAll('.g-recaptcha').forEach(function (el) {
                            grecaptcha.render(el);
                        });
                    }
                </script>
                <br>

                <button type="submit" class="btn btn-primary btn-block">أرسل</button>
            </form>
        </div>
    </div>

    <style>
        ul.infos {
            list-style-type: none;
        }

        ul.infos li {
            margin-bottom: 10px;
        }

        ul.infos li .fa {
            margin-left: 10px;
            margin-bottom: 10px;
        }
    </style>

@endsection