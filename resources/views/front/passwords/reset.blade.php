@extends("front.app")

@section("title", "إعادة تعيين كلمة المرور")
@section("menu", 1)


@section('content')
<div class="container" style="margin-top: 100px">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-danger">
                <div class="panel-heading"><b>إعادة تعيين كلمة المرور</b></div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">البريد الإلكتروني</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">كلمة المرور</label>

                            <div class="col-md-6">
                                <input id="password" type="password" minlength="6" class="form-control" name="password" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">تأكيد كلمة المرور</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" minlength="6" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                {!! NoCaptcha::renderJs('ar', true, 'recaptchaCallback') !!}
                                {!! NoCaptcha::display(['data-theme' => 'dark']) !!}
                                <script>
                                    function recaptchaCallback() {
                                        document.querySelectorAll('.g-recaptcha').forEach(function (el) {
                                            grecaptcha.render(el);
                                        });
                                    }
                                </script>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    إعادة تعيين
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
