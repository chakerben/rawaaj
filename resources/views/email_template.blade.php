<html dir="rtl" lang="ar">
<head>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>
<div class="container" style="text-align: right" dir="rtl">
    <p>{{@$text}}</p>
    <br>
    <a class="btn btn-primary btn-xs" href='{{asset("public/uploads/messages/".@$file)}}'><i class="fa fa-link"></i> ملف
        مرفق</a>
    <br><br>
</div>
</body>
</html>

