<?php

Route::group(["namespace" => "front"], function () {
    Route::get("/", "front@home")->name("home");
    Route::get("/conditions", "front@conditions")->name("conditions");
    Route::get("/about_us", "front@about_us")->name("about_us");
    Route::get("/contact_us", "front@contact_us")->name("contact_us");
    Route::post("/save_contact_us", "front@save_contact_us")->name("save_contact_us");
    Route::get("/dashboard", "Dashboard@index")->name("dashboard");
    Route::post("/dashboard/save_infos", "Dashboard@save_infos")->name("dashboard.save_infos");
    Route::get("/dashboard/product/{id}", "Dashboard@product")->name("dashboard.product");
    Route::get("/dashboard/product_user/{id}", "Dashboard@product_user")->name("dashboard.product_user");
    Route::get("/dashboard/product/ajax/{id?}", "Dashboard@ajax")->name("dashboard.ajax");
    Route::post("/dashboard/send", "Dashboard@send")->name("dashboard.send");
    Route::post("/dashboard/resend/{id}", "Dashboard@resend")->name("dashboard.resend");
    Route::get("/dashboard/show_responses/{id}", "Dashboard@show_responses")->name("dashboard.show_responses");
});

Route::group(["namespace" => "admin","prefix"=>"admin"], function () {
    Route::get("/", "Dashboard@dashboard")->name("admin.dashboard");

    // Auth
    Route::get("/auth/login", "Auth\Authentification@login")->name("admin.auth.login");
    Route::post("/auth/do_login", "Auth\Authentification@do_login")->name("admin.auth.do_login");
    Route::get("/auth/logout", "Auth\Authentification@logout")->name("admin.auth.logout");
    // Forget Password
    Route::post("/auth/password/email", "Auth\AdminForgotPasswordController@sendResetLinkEmail")->name("admin.auth.password.email");
    Route::get("/auth/password/reset", "Auth\AdminForgotPasswordController@showLinkRequestForm")->name("admin.auth.password.request");
    Route::post("/auth/password/reset", "Auth\AdminResetPasswordController@reset");
    Route::get("/auth/password/reset/{token}", "Auth\AdminResetPasswordController@showResetForm")->name("admin.auth.password.reset");
    // Admins
    Route::get("/admins/index", "Admins@index")->name("admin.admins.index");
    Route::get("/admins/create", "Admins@create")->name("admin.admins.create");
    Route::post("/admins/store", "Admins@store")->name("admin.admins.store");
    Route::get("/admins/edite/{id}", "Admins@edit")->name("admin.admins.edit");
    Route::post("/admins/update/{id}", "Admins@update")->name("admin.admins.update");
    Route::get("/admins/delete/{id}", "Admins@delete")->name("admin.admins.delete");
    Route::get("/admins/roles", "Admins@roles")->name("admin.admins.roles");
    Route::post("/admins/save_roles", "Admins@save_roles")->name("admin.admins.save_roles");

    // Users
    Route::get("/users/index", "Users@index")->name("admin.users.index");
    Route::get("/users/create", "Users@create")->name("admin.users.create");
    Route::post("/users/store", "Users@store")->name("admin.users.store");
    Route::get("/users/edite/{id}", "Users@edit")->name("admin.users.edit");
    Route::post("/users/update/{id}", "Users@update")->name("admin.users.update");
    Route::get("/users/delete/{id}", "Users@delete")->name("admin.users.delete");
    Route::get("/users/payments/{id}", "Users@payments")->name("admin.users.payments");
    // Products
    Route::get("/products/index", "Products@index")->name("admin.products.index");
    Route::get("/products/create", "Products@create")->name("admin.products.create");
    Route::post("/products/store", "Products@store")->name("admin.products.store");
    Route::get("/products/edite/{id}", "Products@edit")->name("admin.products.edit");
    Route::post("/products/update/{id}", "Products@update")->name("admin.products.update");
    Route::get("/products/delete/{id}", "Products@delete")->name("admin.products.delete");
    // Settings
    Route::get("/settings", "Settings@index")->name("admin.settings");
    Route::post("/settings/save_content", "Settings@save_content")->name("admin.settings.save_content");
    // Message
    Route::get("/messages/index", "Messages@index")->name("messages.index");
    Route::get("/messages/response/{id}", "Messages@response")->name("messages.response");
    Route::post("/messages/send_response/{id}", "Messages@send_response")->name("messages.send_response");
    Route::get("/messages/delete/{id}", "Messages@delete")->name("messages.delete");
    // Contact
    Route::get("/contacts/index", "Contacts@index")->name("contacts.index");
    Route::get("/contacts/delete/{id}", "Contacts@delete")->name("contacts.delete");
    Route::get("/contacts/show/{id}", "Contacts@show")->name("contacts.show");
    // Inscriptions & Visitors
    Route::get("/inscriptions", "Dashboard@inscriptions")->name("dashboard.inscriptions");
    Route::get("/inscription_status/{id}/{status}", "Dashboard@inscription_status")->name("dashboard.inscription_status");
    Route::post("/inscriptions/filter", "Dashboard@inscriptions_filter")->name("dashboard.inscriptions_filter");
    Route::get("/visitors", "Dashboard@visitors")->name("dashboard.visitors");
    Route::post("/visitors/filter", "Dashboard@visitors_filter")->name("dashboard.visitors_filter");
    // Payments
    Route::get("/payments", "Payments@index")->name("payments.index");
    Route::get("/payments/pay/{id}", "Payments@pay")->name("payments.pay");
    Route::get("/payments/delete/{id}", "Payments@delete")->name("payments.delete");
});

Auth::routes();
Route::get('/register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'Auth\RegisterController@confirm']);
Route::get("/", "front\\front@home")->name("home");
Route::get("/api/save/{t}/{u}/{p}/{n}/{e}/{ip?}", "Api@save");
