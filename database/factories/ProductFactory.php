<?php

use Faker\Factory as Faker;

$factory->define(App\Models\Product::class, function ($faker) {
    $faker = Faker::create('ar_SA');
    return [
        "ref"=>uniqid(),
        "name" => $faker->words(rand(2,3), true),
        "short_description"=>$faker->paragraphs(rand(1,3),true),
        "description"=>$faker->text(600),
        "image"=>$faker->imageUrl(),
        "url"=>$faker->url,
        "status"=>rand(0,1),
        "published"=>rand(0,1)
    ];
});
